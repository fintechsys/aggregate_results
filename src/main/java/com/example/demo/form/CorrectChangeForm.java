package com.example.demo.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.example.demo.validationEdu.First;
import com.example.demo.validationEdu.Second;
import com.example.demo.validationEdu.Third;

/**
 * 正解データ変更フォーム
 * @author tujii
 *
 */
public class CorrectChangeForm {

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 8,groups=Second.class)
	@Pattern(regexp = "[0-9]+",message="半角英数字8文字以下",groups=Third.class)
	private String personalId;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 40,groups=Second.class)
	@Pattern(regexp = "[ぁ-んァ-ン一-龥a-zA-Z0-9]+"+"[ ]"+"[ぁ-んァ-ン一-龥a-zA-Z0-9]+",message="苗字と名前の間に半角スペースを入れてください。",groups=Third.class)
	private String fullName;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 20,groups=Second.class)
	private String birthday;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 100,groups=Second.class)
	private String address;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 16,groups=Second.class)
	private String grantDate;

	@NotEmpty(groups=First.class)
	@Pattern(regexp = "[0-9]{5}",groups=Second.class)
	private String grantNumber;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 20,groups=Second.class)
	private String expirationDate;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 8,groups=Second.class)
	private String licenseColor;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 100,groups=Second.class)
	private String conditions;

	@NotEmpty(groups=First.class)
	@Pattern(regexp = "[0-9]{12}",message="半角数字12文字",groups=Second.class)
	private String licenseNumber;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 16,groups=Second.class)
	private String motorcycleLicense;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 16,groups=Second.class)
	private String others;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 16,groups=Second.class)
	private String commercialLicense;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 50,groups=Second.class)
	private String categorys;

	@NotEmpty(groups=First.class)
	@Size(min = 1, max = 20,groups=Second.class)
	private String publicSafetyCommission;

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGrantDate() {
		return grantDate;
	}

	public void setGrantDate(String grantDate) {
		this.grantDate = grantDate;
	}

	public String getGrantNumber() {
		return grantNumber;
	}

	public void setGrantNumber(String grantNumber) {
		this.grantNumber = grantNumber;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getLicenseColor() {
		return licenseColor;
	}

	public void setLicenseColor(String licenseColor) {
		this.licenseColor = licenseColor;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getMotorcycleLicense() {
		return motorcycleLicense;
	}

	public void setMotorcycleLicense(String motorcycleLicense) {
		this.motorcycleLicense = motorcycleLicense;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getCommercialLicense() {
		return commercialLicense;
	}

	public void setCommercialLicense(String commercialLicense) {
		this.commercialLicense = commercialLicense;
	}

	public String getCategorys() {
		return categorys;
	}

	public void setCategorys(String categorys) {
		this.categorys = categorys;
	}

	public String getPublicSafetyCommission() {
		return publicSafetyCommission;
	}

	public void setPublicSafetyCommission(String publicSafetyCommission) {
		this.publicSafetyCommission = publicSafetyCommission;
	}

}
