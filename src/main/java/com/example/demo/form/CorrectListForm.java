package com.example.demo.form;

/**
 * 正解データ一覧のフォーム
 * @author tujii
 *
 */
public class CorrectListForm {

	private String personalId;

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

}
