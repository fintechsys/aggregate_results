package com.example.demo.form;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import com.example.demo.validationEdu.FileRequired;


public class FileUploadForm implements Serializable {

	@FileRequired
	private MultipartFile uploadedFile;

	public MultipartFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(MultipartFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}



}
