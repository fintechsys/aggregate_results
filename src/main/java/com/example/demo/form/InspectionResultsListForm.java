package com.example.demo.form;

import java.util.List;

/**
 * 検証結果一覧のフォーム
 * @author naomichi nakajima
 *
 */
public class InspectionResultsListForm {

	private String searchByEmpty;

	private List<UpdatePersonalIdForm> updatePersonalIdFormList;

	public String getSearchByEmpty() {
		return searchByEmpty;
	}

	public void setSearchByEmpty(String searchByEmpty) {
		this.searchByEmpty = searchByEmpty;
	}

	public List<UpdatePersonalIdForm> getUpdatePersonalIdFormList() {
		return updatePersonalIdFormList;
	}

	public void setUpdatePersonalIdFormList(List<UpdatePersonalIdForm> updatePersonalIdFormList) {
		this.updatePersonalIdFormList = updatePersonalIdFormList;
	}

}
