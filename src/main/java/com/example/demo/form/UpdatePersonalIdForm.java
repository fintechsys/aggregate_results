package com.example.demo.form;

/**
 * 個人ID修正のフォーム
 *
 * 検証結果一覧のupdatePersonalFormListの中身
 * @author naomichi nakajima
 *
 */
public class UpdatePersonalIdForm {

	private String imagePath;

	private String personalId;

	private String correctFullName;

	public String getCorrectFullName() {
		return correctFullName;
	}

	public void setCorrectFullName(String correctFullName) {
		this.correctFullName = correctFullName;
	}

	public UpdatePersonalIdForm() {

	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

}
