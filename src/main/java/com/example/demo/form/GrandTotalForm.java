package com.example.demo.form;

import org.springframework.web.multipart.MultipartFile;

import com.example.demo.validationEdu.FileRequired;

/**
 * 総計フォーム
 * @author naomichi nakajima
 *
 */
public class GrandTotalForm {

	private String personalId;

	private String angleId;

	@FileRequired
	private MultipartFile uploadedFile;

	public MultipartFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(MultipartFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getAngleId() {
		return angleId;
	}

	public void setAngleId(String angleId) {
		this.angleId = angleId;
	}


}
