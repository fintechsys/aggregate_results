package com.example.demo.form;

/**
 * 正解データ削除フォーム
 * @author tsuji
 *
 */
public class CorrectDeleteForm {

	private String personalId;

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

}
