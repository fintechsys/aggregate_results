package com.example.demo.form;

import java.util.List;

/**
 * 集計一覧フォーム
 * @author naomichi nakajima
 *
 */
public class AggregateListForm {

	private String personalId;

	private String angleId;

	private List<UpdateAngleForm> updateAngleFormList;

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getAngleId() {
		return angleId;
	}

	public void setAngleId(String angleId) {
		this.angleId = angleId;
	}

	public List<UpdateAngleForm> getUpdateAngleFormList() {
		return updateAngleFormList;
	}

	public void setUpdateAngleFormList(List<UpdateAngleForm> updateAngleFormList) {
		this.updateAngleFormList = updateAngleFormList;
	}

}
