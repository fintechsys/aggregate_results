package com.example.demo.form;

/**
 * 角度修正フォーム
 * @author naomichi nakajima
 *
 */
public class UpdateAngleForm {

	private String imagePath;

	private String angleId;

	private String angle;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getAngleId() {
		return angleId;
	}

	public void setAngleId(String angleId) {
		this.angleId = angleId;
	}

	public String getAngle() {
		return angle;
	}

	public void setAngle(String angle) {
		this.angle = angle;
	}
}
