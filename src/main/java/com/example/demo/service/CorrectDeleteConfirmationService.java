package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.CorrectDeleteDto;
import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.form.CorrectDeleteConfirmationForm;
import com.example.demo.repository.CorrectDataRepository;
import com.example.demo.util.Utility;

@Service
public class CorrectDeleteConfirmationService {

	@Autowired
	CorrectDataRepository correctDataRepository;

	/**
	 * 個人IDをもとに、削除したい正解データをリポジトリから受け取るメソッド
	 * @param personalId
	 * @return 削除する正解データ
	 */
	public CorrectDeleteDto correctDeleteData(String personalId) {

		CorrectDataEntity correctDataEntity = correctDataRepository.findByPersonalIdCorrectData(personalId);

		CorrectDeleteDto correctDeleteDto = new CorrectDeleteDto();

		entityConvertToDto(correctDeleteDto, correctDataEntity);

		return correctDeleteDto;

	}

	/**
	 * 正解データを論理削除するメソッド
	 * @param personalId
	 */
	@Transactional
	public void deleteCorrectData(CorrectDeleteConfirmationForm confirmationForm) {
		correctDataRepository.deleteCorrectData(confirmationForm.getPersonalId());
	}

	/**
	 * エンティティからDtoに置き換えるメソッド
	 * @param correctDataDtoList
	 * @param correctDataEntityList
	 */
	private void entityConvertToDto(CorrectDeleteDto correctDeleteDto,
			CorrectDataEntity correctDataEntity) {

		Utility.correctDataEntityConvertToDto(correctDeleteDto, correctDataEntity);

	}

}
