package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.InspectionResultsJoinCorrectDataJoinAngleDto;
import com.example.demo.dto.SearchCorrectDataForAggregateListDto;
import com.example.demo.entity.AngleEntity;
import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.entity.InspectionResultsJoinCorrectDataJoinAngleEntity;
import com.example.demo.form.AggregateListForm;
import com.example.demo.form.UpdateAngleForm;
import com.example.demo.repository.AngleRepository;
import com.example.demo.repository.CorrectDataRepository;
import com.example.demo.repository.InspectionResultsJoinCorrectDataJoinAngleRepository;
import com.example.demo.repository.InspectionResultsRepository;
import com.example.demo.util.Utility;

/**
 * 集計一覧サービス
 *
 * @author nakajima
 *
 */
@Service
public class AggregateListService {

	@Autowired
	InspectionResultsJoinCorrectDataJoinAngleRepository inspectionResultsJoinCorrectDataJoinAngleRepository;

	@Autowired
	AngleRepository angleRepository;

	@Autowired
	InspectionResultsRepository inspectionResultsRepository;

	@Autowired
	CorrectDataRepository correctDataRepository;

	/**
	 * 集計一覧Dtoから正解データを取得
	 * @return 集計一覧DtoList
	 */
	public List<SearchCorrectDataForAggregateListDto> findByCorrectData() {
		List<SearchCorrectDataForAggregateListDto> aggregateListDtoList = new ArrayList<>();
		List<CorrectDataEntity> correctDataEntityList = correctDataRepository.findByAllCorrectDataList();
		correctDataEntityConvertToDto(aggregateListDtoList, correctDataEntityList);
		return aggregateListDtoList;
	}

	/**
	 * 角度のEntityからデータを取得
	 * @return angleEntityList
	 */
	public List<AngleEntity> findByAllAngle() {
		return angleRepository.findByAllAngle();
	}

	/**
	 * 角度のIDを修正するメソッド
	 * @param form
	 */
	@Transactional
	public void updateAngleId(AggregateListForm form) {
		for (UpdateAngleForm updateAngleForm : form.getUpdateAngleFormList()) {
			inspectionResultsRepository.updateAngle(updateAngleForm.getAngleId(), updateAngleForm.getImagePath());
		}
	}

	/**
	 * 正解データと検証結果データと角度テーブルを結合した情報のDto
	 * @param form
	 * @return InspectionResultsJoinCorrectDataJoinAngleDtoList
	 */
	public List<InspectionResultsJoinCorrectDataJoinAngleDto> findByAggregateList() {
		List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsJoinCorrectDataJoinAngleDtoList = new ArrayList<InspectionResultsJoinCorrectDataJoinAngleDto>();
		List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
				.findByAllAggregateList();

		inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(inspectionResultsJoinCorrectDataJoinAngleDtoList,
				inspectionResultsJoinCorrectDataJoinAngleEntitiyList);

		return inspectionResultsJoinCorrectDataJoinAngleDtoList;
	}

	/**
	 * 個人IDと角度IDをもとに検索するメソッド
	 * @param form
	 * @return List<InspectionResultsJoinCorrectDataJoinAngleDto>
	 */
	public List<InspectionResultsJoinCorrectDataJoinAngleDto> searchByAggregateList(AggregateListForm form) {

		List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsJoinCorrectDataJoinAngleDtoList = new ArrayList<InspectionResultsJoinCorrectDataJoinAngleDto>();

		if ((!(form.getPersonalId() == null) && form.getAngleId() == null)
				|| (!(form.getPersonalId().isEmpty()) && form.getAngleId().isEmpty())) {
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
					.findByPersonalIdForAggregateList(form.getPersonalId());
			inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(inspectionResultsJoinCorrectDataJoinAngleDtoList,
					inspectionResultsJoinCorrectDataJoinAngleEntitiyList);

			return inspectionResultsJoinCorrectDataJoinAngleDtoList;
		}

		else if ((!(form.getAngleId() == null) && form.getPersonalId() == null)
				|| (!(form.getAngleId().isEmpty()) && form.getPersonalId().isEmpty())) {
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
					.findByAllAngleIdForAggregateList(form.getAngleId());
			inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(inspectionResultsJoinCorrectDataJoinAngleDtoList,
					inspectionResultsJoinCorrectDataJoinAngleEntitiyList);

			return inspectionResultsJoinCorrectDataJoinAngleDtoList;
		} else {
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
					.findByAllPersonalIdAndAngleIdForAggregateLIst(form.getPersonalId(), form.getAngleId());
			inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(inspectionResultsJoinCorrectDataJoinAngleDtoList,
					inspectionResultsJoinCorrectDataJoinAngleEntitiyList);

			return inspectionResultsJoinCorrectDataJoinAngleDtoList;
		}
	}

	/**
	 * 検索する際、正解の値をセレクトボックスへ表示するため
	 * 正解データEntityをDtoに格納するメソッド
	 * @param aggregateListDtoList
	 * @param correctDataEntityList
	 * @return  List<SearchCorrectDataForAggregateListDto>
	 */
	private List<SearchCorrectDataForAggregateListDto> correctDataEntityConvertToDto(
			List<SearchCorrectDataForAggregateListDto> aggregateListDtoList,
			List<CorrectDataEntity> correctDataEntityList) {

		for (CorrectDataEntity correctDataEntity : correctDataEntityList) {
			SearchCorrectDataForAggregateListDto searchCorrectDataForAggregateListDto = new SearchCorrectDataForAggregateListDto();

			searchCorrectDataForAggregateListDto.setPersonalId(correctDataEntity.getPersonalId());
			searchCorrectDataForAggregateListDto.setFullName(correctDataEntity.getFullName());
			searchCorrectDataForAggregateListDto.setBirthday(correctDataEntity.getBirthday());
			searchCorrectDataForAggregateListDto.setAddress(correctDataEntity.getAddress());
			searchCorrectDataForAggregateListDto.setGrantDate(correctDataEntity.getGrantDate());
			searchCorrectDataForAggregateListDto.setGrantNumber(correctDataEntity.getGrantNumber());
			searchCorrectDataForAggregateListDto.setExpirationDate(correctDataEntity.getExpirationDate());
			searchCorrectDataForAggregateListDto.setLicenseColor(correctDataEntity.getLicenseColor());
			searchCorrectDataForAggregateListDto.setConditions(correctDataEntity.getConditions());
			searchCorrectDataForAggregateListDto.setLicenseNumber(correctDataEntity.getLicenseNumber());
			searchCorrectDataForAggregateListDto.setMotorcycleLicense(correctDataEntity.getMotorcycleLicense());
			searchCorrectDataForAggregateListDto.setOthers(correctDataEntity.getOthers());
			searchCorrectDataForAggregateListDto.setCommercialLicense(correctDataEntity.getCommercialLicense());
			searchCorrectDataForAggregateListDto.setCategorys(correctDataEntity.getCategorys());
			searchCorrectDataForAggregateListDto
					.setPublicSafetyCommission(correctDataEntity.getPublicSafetyCommission());

			aggregateListDtoList.add(searchCorrectDataForAggregateListDto);
		}
		return aggregateListDtoList;
	}

	public void inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(
			List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsJoinCorrectDataJoinAngleDtoList,
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {
		Utility.inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(
				inspectionResultsJoinCorrectDataJoinAngleDtoList, inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
	}






}
