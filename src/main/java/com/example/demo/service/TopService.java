package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.form.FileUploadForm;
import com.example.demo.repository.InspectionResultsRepository;
import com.example.demo.util.PropertyUtil;

/**
 * TOPサービス
 *
 * @author naomichi nakajima
 *
 */

@Service
public class TopService {

	@Autowired
	InspectionResultsRepository inspectionResultsRepository;

	/**
	 * ファイルをアップロードするメソッド
	 */
	@Transactional
	public void fileUpload(FileUploadForm form) {
		inspectionResultsRepository
				.fileUpload(PropertyUtil.getProperty("filePath") + form.getUploadedFile().getOriginalFilename());

	}

}
