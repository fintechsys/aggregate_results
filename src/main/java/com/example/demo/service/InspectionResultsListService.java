package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.InspectionResultsJoinCorrectDataJoinAngleDto;
import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.entity.InspectionResultsJoinCorrectDataJoinAngleEntity;
import com.example.demo.form.InspectionResultsListForm;
import com.example.demo.form.UpdatePersonalIdForm;
import com.example.demo.repository.CorrectDataRepository;
import com.example.demo.repository.InspectionResultsJoinCorrectDataJoinAngleRepository;
import com.example.demo.repository.InspectionResultsRepository;
import com.example.demo.util.Utility;

/**
 * 検証結果一覧のサービス
 *
 * @author naomichi nakajima
 *
 */
@Service
public class InspectionResultsListService {

	@Autowired
	InspectionResultsRepository inspectionResultsRepository;

	@Autowired
	CorrectDataRepository correctDataRepository;

	@Autowired
	InspectionResultsJoinCorrectDataJoinAngleRepository inspectionResultsJoinCorrectDataRepository;

	/**
	 * 検証結果データと正解の氏名のDtoを全件取得
	 * @return 検証結果データと正解データを結合したDtoList
	 */
	public List<InspectionResultsJoinCorrectDataJoinAngleDto> findByAllInspectionResults() {

		List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsJoinCorrectDataJoinAngleDtoList = new ArrayList<>();
		List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataRepository
				.findByAllInspectionResultsJoinCorrectData();

		Utility.entityConvertToDto(
				inspectionResultsJoinCorrectDataJoinAngleDtoList, inspectionResultsJoinCorrectDataJoinAngleEntitiyList);

		System.out.println(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
		return inspectionResultsJoinCorrectDataJoinAngleDtoList;

	}

	/**
	 * 検証結果データと正解の氏名のDtoから個人IDが未入力のものを取得
	 * @return 検証結果データと正解データを結合したDtoList
	 */
	public List<InspectionResultsJoinCorrectDataJoinAngleDto> findByEmptyPrsonalId() {
		List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsJoinCorrectDataJoinAngleDtoList = new ArrayList<>();
		List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntityList = inspectionResultsJoinCorrectDataRepository
				.findByEmptyPrsonalId();

		Utility.entityConvertToDto(
				inspectionResultsJoinCorrectDataJoinAngleDtoList, inspectionResultsJoinCorrectDataJoinAngleEntityList);
		return inspectionResultsJoinCorrectDataJoinAngleDtoList;
	}

	/**
	 * 正解データの個人IDを取得
	 * @return CorrectDataEntityList
	 */
	public List<CorrectDataEntity> findByPersonalIdAndFullName() {
		return correctDataRepository.findByAllCorrectDataList();
	}

	/**
	 * 検証結果データの個人IDを修正するメソッド
	 * @param inspectionResultsListFormList
	 */
	@Transactional
	public void updatePersonalId(InspectionResultsListForm inspectionResultsListFormList) {

		for (UpdatePersonalIdForm inspectionResultsListForm : inspectionResultsListFormList
				.getUpdatePersonalIdFormList()) {
			inspectionResultsRepository.updatePersonalId(inspectionResultsListForm.getPersonalId(),
					inspectionResultsListForm.getImagePath());
		}
	}

}
