package com.example.demo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.RecognitionResultDto;
import com.example.demo.dto.StatusDataDto;
import com.example.demo.entity.StatusDataEntity;
import com.example.demo.form.GrandTotalForm;
import com.example.demo.repository.StatusDataRepository;

/**
 * 認証結果サービス
 * @author nakajima
 *
 */
@Service
public class RecognitionResultService {

	@Autowired
	StatusDataRepository statusDataRepository;

	/**
	 * ファイルアップロードのメソッド
	 */
	@Transactional
	public void fileUpload(GrandTotalForm form) {
		statusDataRepository.fileUpload("C:/Users/nakajima/Desktop/" + form.getUploadedFile().getOriginalFilename());
	}

	/**
	 * 検証状態データエンティティを全件取得するメソッド
	 * @return List<StatusDataEntity>
	 */
	public List<StatusDataEntity> findByAllStatusData() {
		return statusDataRepository.findByAllStatusData();
	}

	/**
	 * 必要な認証データをDtoに格納
	 * @return recognitionResultDto
	 */
	public RecognitionResultDto findByRecognitionResult() {
		RecognitionResultDto recognitionResultDto = new RecognitionResultDto();
		List<StatusDataEntity> statusDataEntityList = statusDataRepository.findByAllStatusData();

		recognitionResultDto.setStatusDataDtoList(statusDataEntityConvertToRecognitionResultDto(statusDataEntityList));
		recognitionResultDto.setRecognitionResultCount(recognitionResultCount(statusDataEntityList));
		recognitionResultDto.setRecognitionResultRatio(recognitionResultRatio(statusDataEntityList));

		return recognitionResultDto;
	}

	/**
	 * 検証状態データEntityをDtoに格納
	 * @return statusDataDtoList
	 */
	public List<StatusDataDto> statusDataEntityConvertToRecognitionResultDto(
			List<StatusDataEntity> statusDataEntityList) {
		List<StatusDataDto> statusDataDtoList = new ArrayList<StatusDataDto>();

		for (StatusDataEntity statusDataEntity : statusDataEntityList) {
			StatusDataDto statusDataDto = new StatusDataDto();
			statusDataDto.setImagePath(statusDataEntity.getImagePath());
			statusDataDto.setStatus(statusDataEntity.getStatus());

			statusDataDtoList.add(statusDataDto);
		}
		return statusDataDtoList;
	}

	/**
	 * 認証率を計算するメソッド
	 * @return recognitionResultRatio
	 */
	public BigDecimal recognitionResultRatio(List<StatusDataEntity> statusDataEntityList) {
		double recognitionResultCount = (double) recognitionResultCount(statusDataEntityList);
		double d = recognitionResultCount / statusDataEntityList.size() * 100;
		BigDecimal bd = new BigDecimal(d);
		BigDecimal recognitionResultRatio = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		return recognitionResultRatio;

	}

	/**
	 * 認証結果がOKのものだけカウントするメソッド
	 * @return recognitionResultCount
	 */
	public Integer recognitionResultCount(List<StatusDataEntity> statusDataEntityList) {
		Integer recognitionResultCount = 0;

		for (StatusDataEntity statusDataEntity : statusDataEntityList) {
			if (statusDataEntity.getStatus().equals("OK")) {
				recognitionResultCount++;
			}
		}
		return recognitionResultCount;
	}

}
