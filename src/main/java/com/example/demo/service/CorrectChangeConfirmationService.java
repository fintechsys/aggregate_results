package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.form.CorrectChangeConfirmationForm;
import com.example.demo.repository.CorrectDataRepository;

@Service
public class CorrectChangeConfirmationService {

	@Autowired
	CorrectDataRepository correctDataRepository;

	/**
	 * 変更する正解データをリポジトリに送るメソッド
	 * @param confirmationForm
	 */
	@Transactional
	public void changeCorrectData(CorrectChangeConfirmationForm confirmationForm) {

		correctDataRepository.changeCorrectData(
				confirmationForm.getPersonalId(),
				confirmationForm.getFullName(),
				confirmationForm.getBirthday(),
				confirmationForm.getAddress(),
				confirmationForm.getGrantDate(),
				confirmationForm.getGrantNumber(),
				confirmationForm.getExpirationDate(),
				confirmationForm.getLicenseColor(),
				confirmationForm.getConditions(),
				confirmationForm.getLicenseNumber(),
				confirmationForm.getMotorcycleLicense(),
				confirmationForm.getOthers(),
				confirmationForm.getCommercialLicense(),
				confirmationForm.getCategorys(),
				confirmationForm.getPublicSafetyCommission());

	}

}
