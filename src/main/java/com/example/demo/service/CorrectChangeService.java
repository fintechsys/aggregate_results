package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.form.CorrectChangeForm;
import com.example.demo.repository.CorrectDataRepository;
import com.example.demo.util.Utility;

/**
 * 正解データ変更のサービス
 * @author tujii
 *
 */
@Service
public class CorrectChangeService {

	@Autowired
	CorrectDataRepository correctDataRepository;

	/**
	 * 個人IDをもとに、変更したい正解データをリポジトリから受け取るメソッド
	 * @param personalId
	 * @return 変更する正解データ
	 */
	public CorrectChangeForm correctChangeData(String personalId, CorrectChangeForm correctChangeForm) {

		CorrectDataEntity correctEntity = correctDataRepository.findByPersonalIdCorrectData(personalId);

		this.entityConvertToForm(correctChangeForm, correctEntity);

		return correctChangeForm;

	}

	/**
	 * エンティティからフォームに置き換えるメソッド
	 * @param correctChangeForm
	 * @param correctDataEntity
	 */
	private void entityConvertToForm(CorrectChangeForm correctChangeForm, CorrectDataEntity correctDataEntity) {

		Utility.CorrectDataEntityConvertToForm(correctChangeForm, correctDataEntity);
	}

}
