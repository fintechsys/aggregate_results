package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CorrectListDto;
import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.form.CorrectListForm;
import com.example.demo.repository.CorrectDataRepository;
import com.example.demo.util.Utility;

/**
 * 正解データ一覧のサービス
 * @author tujii
 *
 */
@Service
public class CorrectListService {

	@Autowired
	CorrectDataRepository correctDataRepository;

	/**
	 * 個人IDの有無で正解データ全件取得・条件取得するメソッド
	 * @return 正解データ全件・条件別を入れたDtoリスト
	 */
	public List<CorrectListDto> findByAllCorrectDataDtoList(CorrectListForm form) {

		List<CorrectListDto> correctListDtoList = new ArrayList<CorrectListDto>();

		List<CorrectDataEntity> correctDataEntityList = new ArrayList<CorrectDataEntity>();
		if (null == form.getPersonalId() || form.getPersonalId().isEmpty()) {
			correctDataEntityList = correctDataRepository.findByAllCorrectDataList();
		} else {
			correctDataEntityList = correctDataRepository.findByPersonalIdCorrectDataList(form.getPersonalId());
		}

		entityConvertToDto(correctListDtoList, correctDataEntityList);

		return correctListDtoList;

	}

	/**
	 * セレクトボックスの中身を取得するメソッド
	 * @return 正解データ全件(個人IDと名前使うため)
	 */
	public List<CorrectListDto> selectList() {

		List<CorrectListDto> correctListDtoList = new ArrayList<CorrectListDto>();

		List<CorrectDataEntity> correctDataEntityList = correctDataRepository.findByAllCorrectDataList();

		entityConvertToDto(correctListDtoList, correctDataEntityList);

		return correctListDtoList;

	}

	/**
	 * エンティティからDtoに置き換えるメソッド
	 * @param correctDataDtoList
	 * @param correctDataEntityList
	 */
	private void entityConvertToDto(List<CorrectListDto> correctDataDtoList,
			List<CorrectDataEntity> correctDataEntityList) {

		Utility.correctDataEntityListConvertToDto(correctDataDtoList, correctDataEntityList);
	}

}
