package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.form.CorrectRegisterConfirmationForm;
import com.example.demo.repository.CorrectDataRepository;
import com.example.demo.repository.SequenceRepository;

/**
 * 正解データ登録確認のサービス
 * @author tujii
 *
 */
@Service
public class CorrectRegisterConfirmationService {

	@Autowired
	CorrectDataRepository correctDataRepository;

	@Autowired
	SequenceRepository sequenceRepository;

	/**
	 * 正解データをリポジトリに送るメソッド
	 * @param confirmationForm
	 * @throws Exception
	 */
	@Transactional
	public void registerCorrectData(CorrectRegisterConfirmationForm confirmationForm) throws Exception {


		String beforPersonalId = sequenceRepository.nextPersonalId();
		if (correctDataRepository.findByAllCorrectDataList().isEmpty()) {
			sequenceRepository.setPersonalId();
			sequenceRepository.setinitial('A');
		}
		char initial = sequenceRepository.initial();

		if (beforPersonalId.equals("9999999")) {
				initial++;
				if (initial=='[') {
					throw new Exception();
				}
				sequenceRepository.setPersonalId();
				sequenceRepository.setinitial(initial);
		}

		String personalId = sequenceRepository.updatePersonalId();
		correctDataRepository.insertCorrectData(
				String.valueOf(initial)+personalId,
				confirmationForm.getFullName(),
				confirmationForm.getBirthday(),
				confirmationForm.getAddress(),
				confirmationForm.getGrantDate(),
				confirmationForm.getGrantNumber(),
				confirmationForm.getExpirationDate(),
				confirmationForm.getLicenseColor(),
				confirmationForm.getConditions(),
				confirmationForm.getLicenseNumber(),
				confirmationForm.getMotorcycleLicense(),
				confirmationForm.getOthers(),
				confirmationForm.getCommercialLicense(),
				confirmationForm.getCategorys(),
				confirmationForm.getPublicSafetyCommission());

	}

}
