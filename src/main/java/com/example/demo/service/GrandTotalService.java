package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.GrandTotalDto;
import com.example.demo.dto.InspectionResultsJoinCorrectDataJoinAngleDto;
import com.example.demo.entity.AngleEntity;
import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.entity.InspectionResultsJoinCorrectDataJoinAngleEntity;
import com.example.demo.form.GrandTotalForm;
import com.example.demo.repository.AngleRepository;
import com.example.demo.repository.CorrectDataRepository;
import com.example.demo.repository.InspectionResultsJoinCorrectDataJoinAngleRepository;
import com.example.demo.util.Utility;

/**
 * 総計サービス
 * @author naomichi nakajima
 *
 */
@Service
public class GrandTotalService {

	@Autowired
	InspectionResultsJoinCorrectDataJoinAngleRepository inspectionResultsJoinCorrectDataJoinAngleRepository;

	@Autowired
	CorrectDataRepository correctDataRepository;

	@Autowired
	AngleRepository angleRepository;

	/**
	 * 集計一覧Dtoから正解データを取得
	 * @return 集計一覧DtoList
	 */
	public List<CorrectDataEntity> findByCorrectData() {
		return correctDataRepository.findByAllCorrectDataList();
	}

	/**
	 * 角度のEntityからデータを取得
	 * @return angleEntityList
	 */
	public List<AngleEntity> findByAllAngle() {
		return angleRepository.findByAllAngle();
	}

	/**
	 * 正解データと検証結果データと角度テーブルを結合した情報のDtoを全件取得
	 * @param form
	 * @return InspectionResultsJoinCorrectDataJoinAngleDtoList
	 */
	public List<InspectionResultsJoinCorrectDataJoinAngleDto> findByAggregateList() {
		List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsJoinCorrectDataDtoList = new ArrayList<InspectionResultsJoinCorrectDataJoinAngleDto>();
		List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
				.findByAllAggregateList();

		inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(inspectionResultsJoinCorrectDataDtoList,
				inspectionResultsJoinCorrectDataJoinAngleEntitiyList);

		return inspectionResultsJoinCorrectDataDtoList;
	}

	/**
	 * 初期表示するために検証結果データ、正解データ、角度テーブルを結合し全件取得
	 * @return GrandTotalDto
	 */
	public GrandTotalDto findByAllGrandTotal() {
		List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
				.findByAllAggregateList();

		return totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
	}

	/**
	 * 個人IDと角度IDをもとに検索するメソッド
	 * @param form
	 * @return List<InspectionResultsJoinCorrectDataJoinAngleDto>
	 */
	public GrandTotalDto searchByGrandTotal(GrandTotalForm form) {

		if ((!(form.getPersonalId() == null) && form.getAngleId() == null)
				|| (!(form.getPersonalId().isEmpty()) && form.getAngleId().isEmpty())) {
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
					.findByPersonalIdForAggregateList(form.getPersonalId());

			return totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
		}

		//		全体正面、全体縦、全体台形、全体斜めを取得するためにsplitで分割して引数を渡している
		else if ((!(form.getAngleId() == null) && form.getPersonalId() == null)
				|| (!(form.getAngleId().isEmpty()) && form.getPersonalId().isEmpty())) {
			if (form.getAngleId().contains("_")) {
				String[] angleId = form.getAngleId().split("_");
				List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
						.findByAllFourAngleIdForAggregateList(angleId[0], angleId[1], angleId[2], angleId[3]);
				return totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
			} else if (form.getAngleId().contains(",")) {
				String[] angleId = form.getAngleId().split(",");
				List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
						.findByAllTwoAngleIdForAggregateList(angleId[0], angleId[1]);
				return totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
			} else {
				List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
						.findByAllAngleIdForAggregateList(form.getAngleId());
				return totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
			}

			//		全体正面、全体縦、全体台形、全体斜めを取得するためにsplitで分割して引数を渡している
		} else {
			if (form.getAngleId().contains("_")) {
				String[] angleId = form.getAngleId().split("_");
				List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
						.findByAllPersonalIdAndFourAngleIdForAggregateList(form.getPersonalId(), angleId[0], angleId[1],
								angleId[2], angleId[3]);
				return totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
			} else if (form.getAngleId().contains(",")) {
				String[] angleId = form.getAngleId().split(",");
				List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
						.findByAllPersonalIdAndTwoAngleIdForAggregateList(form.getPersonalId(), angleId[0], angleId[1]);
				return totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
			} else {
				List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList = inspectionResultsJoinCorrectDataJoinAngleRepository
						.findByAllPersonalIdAndAngleIdForAggregateLIst(form.getPersonalId(), form.getAngleId());
				return totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
			}

		}
	}

	/**
	 * 一致率と識字率の割合ごとの件数と
	 * 総合的な一致率と識字率をDtoに格納し取得するメソッド
	 * @param inspectionResultsJoinCorrectDataJoinAngleEntitiyList
	 * @return grandTotalDto
	 */
	public GrandTotalDto totalCount(
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {
		return Utility.totalCount(inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
	}

	/**
	 *
	 * 正解データと検証結果データと角度テーブルを結合したEntityをDtoに格納トするメソッド
	 * また文字数や識字率に必要なものをEntityから処理してDtoに格納
	 * @param inspectionResultsJoinCorrectDataJoinAngleDtoList
	 * @param inspectionResultsJoinCorrectDataJoinAngleEntitiyList
	 */
	public void inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(
			List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsJoinCorrectDataJoinAngleDtoList,
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {

		Utility.inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(
				inspectionResultsJoinCorrectDataJoinAngleDtoList, inspectionResultsJoinCorrectDataJoinAngleEntitiyList);
	}

}
