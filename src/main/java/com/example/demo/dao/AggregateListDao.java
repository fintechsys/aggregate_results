package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.dto.InspectionResultsJoinCorrectDataJoinAngleDto;
import com.example.demo.form.AggregateListForm;
import com.example.demo.model.AggregateListModel;
import com.example.demo.service.AggregateListService;

/**
 * 集計一覧Dao
 * @author naomichi nakajima
 *
 */
public class AggregateListDao {

	@Autowired
	AggregateListService aggregateListService;

	/**
	 * pdfで出力するデータを設定するメソッド
	 * @param form
	 * @param DtoList
	 * @return List<AggregateListModel>
	 */
	public List<AggregateListModel> findByAll(AggregateListForm form,
			List<InspectionResultsJoinCorrectDataJoinAngleDto> DtoList) {
		List<AggregateListModel> result = new ArrayList<>();

		AggregateListModel[] data = new AggregateListModel[DtoList.size()];
		int i = 0;
		for (InspectionResultsJoinCorrectDataJoinAngleDto dto : DtoList) {

			data[i] = new AggregateListModel();
			data[i].setImagePath(dto.getImagePath());
			data[i].setCorrectFullName(dto.getCorrectFullName());
			data[i].setCorrectBirthday(dto.getCorrectBirthday());
			data[i].setCorrectAddress(dto.getCorrectAddress());
			data[i].setCorrectGrantDate(dto.getCorrectGrantDate());
			data[i].setCorrectGrantNumber(dto.getCorrectGrantNumber());
			data[i].setCorrectExpirationDate(dto.getCorrectExpirationDate());
			data[i].setCorrectLicenseColor(dto.getCorrectLicenseColor());
			data[i].setCorrectConditions(dto.getCorrectConditions());
			data[i].setCorrectLicenseNumber(dto.getCorrectLicenseNumber());
			data[i].setCorrectMotorcycleLicense(dto.getCorrectMotorcycleLicense());
			data[i].setCorrectOthers(dto.getCorrectOthers());
			data[i].setCorrectCommercialLicense(dto.getCorrectCommercialLicense());
			data[i].setCorrectCategorys(dto.getCorrectCategorys());
			data[i].setCorrectPublicSafetyCommission(dto.getCorrectPublicSafetyCommission());
			data[i].setInspectionFullName(dto.getInspectionFullName());
			data[i].setInspectionBirthday(dto.getInspectionBirthday());
			data[i].setInspectionAddress(dto.getInspectionAddress());
			data[i].setInspectionGrantDate(dto.getInspectionGrantDate());
			data[i].setInspectionGrantNumber(dto.getInspectionGrantNumber());
			data[i].setInspectionExpirationDate(dto.getInspectionExpirationDate());
			data[i].setInspectionLicenseColor(dto.getInspectionLicenseColor());
			data[i].setInspectionConditions(dto.getInspectionConditions());
			data[i].setInspectionLicenseNumber(dto.getInspectionLicenseNumber());
			data[i].setInspectionMotorcycleLicense(dto.getInspectionMotorcycleLicense());
			data[i].setInspectionOthers(dto.getInspectionOthers());
			data[i].setInspectionCommercialLicense(dto.getInspectionCommercialLicense());
			data[i].setInspectionCategorys(dto.getInspectionCategorys());
			data[i].setInspectionPublicSafetyCommission(dto.getInspectionPublicSafetyCommission());
			data[i].setResearchMacth(researchMacth(dto.getCorrectFullName(), dto.getInspectionFullName()));
			data[i].setResearchMacth1(researchMacth(dto.getCorrectBirthday(), dto.getInspectionBirthday()));

			data[i].setResearchMacth2(researchMacth(dto.getCorrectAddress(), dto.getInspectionAddress()));
			data[i].setResearchMacth3(researchMacth(dto.getCorrectGrantDate(), dto.getInspectionGrantDate()));
			data[i].setResearchMacth4(researchMacth(dto.getCorrectGrantNumber(), dto.getInspectionGrantNumber()));
			data[i].setResearchMacth5(
					researchMacth(dto.getCorrectExpirationDate(), dto.getInspectionExpirationDate()));
			data[i].setResearchMacth6(researchMacth(dto.getCorrectLicenseColor(), dto.getInspectionLicenseColor()));
			data[i].setResearchMacth7(researchMacth(dto.getCorrectConditions(), dto.getInspectionConditions()));
			data[i].setResearchMacth8(
					researchMacth(dto.getCorrectLicenseNumber(), dto.getInspectionLicenseNumber()));
			data[i].setResearchMacth9(
					researchMacth(dto.getCorrectMotorcycleLicense(), dto.getInspectionMotorcycleLicense()));
			data[i].setResearchMacth10(researchMacth(dto.getCorrectOthers(), dto.getCorrectOthers()));
			data[i].setResearchMacth11(
					researchMacth(dto.getCorrectCommercialLicense(), dto.getInspectionCommercialLicense()));
			data[i].setResearchMacth12(researchMacth(dto.getCorrectCategorys(), dto.getInspectionCategorys()));
			data[i].setResearchMacth13(
					researchMacth(dto.getCorrectPublicSafetyCommission(), dto.getInspectionPublicSafetyCommission()));

			data[i].setCorrectFullNameLength(dto.getLiteracyRateDtoList().get(i).getCorrectFullNameLength());
			data[i].setCorrectBirthdayLength(dto.getLiteracyRateDtoList().get(i).getCorrectBirthdayLength());
			data[i].setCorrectAddressLength(dto.getLiteracyRateDtoList().get(i).getCorrectAddressLength());
			data[i].setCorrectGrantDateLength(dto.getLiteracyRateDtoList().get(i).getCorrectGrantDateLength());
			data[i].setCorrectGrantNumberLength(dto.getLiteracyRateDtoList().get(i).getCorrectGrantNumberLength());
			data[i].setCorrectExpirationDateLength(dto.getLiteracyRateDtoList().get(i).getCorrectExpirationDateLength());
			data[i].setCorrectLicenseColorLength(dto.getLiteracyRateDtoList().get(i).getCorrectLicenseColorLength());
			data[i].setCorrectConditionsLength(dto.getLiteracyRateDtoList().get(i).getCorrectConditionsLength());
			data[i].setCorrectLicenseNumberLength(dto.getLiteracyRateDtoList().get(i).getCorrectLicenseNumberLength());
			data[i].setCorrectMotorcycleLicenseLength(dto.getLiteracyRateDtoList().get(i).getCorrectMotorcycleLicenseLength());
			data[i].setCorrectOthersLength(dto.getLiteracyRateDtoList().get(i).getCorrectOthersLength());
			data[i].setCorrectCommercialLicenseLength(dto.getLiteracyRateDtoList().get(i).getCorrectCommercialLicenseLength());
			data[i].setCorrectCategorysLength(dto.getLiteracyRateDtoList().get(i).getCorrectCategorysLength());
			data[i].setCorrectPublicSafetyCommissionLength(dto.getLiteracyRateDtoList().get(i).getCorrectPublicSafetyCommissionLength());

			data[i].setCorrectAnswerFullNameLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerFullNameLength());
			data[i].setCorrectAnswerBirthdayLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerBirthdayLength());
			data[i].setCorrectAnswerAddressLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerAddressLength());
			data[i].setCorrectAnswerGrantDateLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerGrantDateLength());
			data[i].setCorrectAnswerGrantNumberLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerGrantNumberLength());
			data[i].setCorrectAnswerExpirationDateLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerExpirationDateLength());
			data[i].setCorrectAnswerLicenseColorLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerLicenseColorLength());
			data[i].setCorrectAnswerConditionsLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerConditionsLength());
			data[i].setCorrectAnswerLicenseNumberLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerLicenseNumberLength());
			data[i].setCorrectAnswerMotorcycleLicenseLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerMotorcycleLicenseLength());
			data[i].setCorrectAnswerOthersLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerOthersLength());
			data[i].setCorrectAnswerCommercialLicenseLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerCommercialLicenseLength());
			data[i].setCorrectAnswerCategorysLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerCategorysLength());
			data[i].setCorrectAnswerPublicSafetyCommissionLength(dto.getLiteracyRateDtoList().get(i).getCorrectAnswerPublicSafetyCommissionLength());

			data[i].setTotalCorrectLength(dto.getLiteracyRateDtoList().get(i).getTotalCorrectLength());
			data[i].setTotalCorrectAnswerLength(dto.getLiteracyRateDtoList().get(i).getTotalCorrectAnswerLength());

			data[i].setFullnameLiteracyRate(dto.getLiteracyRateDtoList().get(i).getFullnameLiteracyRate());
			data[i].setBirthdayLiteracyRate(dto.getLiteracyRateDtoList().get(i).getBirthdayLiteracyRate());
			data[i].setAddressLiteracyRate(dto.getLiteracyRateDtoList().get(i).getAddressLiteracyRate());
			data[i].setGrantDateLiteracyRate(dto.getLiteracyRateDtoList().get(i).getGrantDateLiteracyRate());
			data[i].setGrantNumberLiteracyRate(dto.getLiteracyRateDtoList().get(i).getGrantNumberLiteracyRate());
			data[i].setExpirationDateLiteracyRate(dto.getLiteracyRateDtoList().get(i).getExpirationDateLiteracyRate());
			data[i].setLicenseColorLiteracyRate(dto.getLiteracyRateDtoList().get(i).getLicenseColorLiteracyRate());
			data[i].setConditionsLiteracyRate(dto.getLiteracyRateDtoList().get(i).getConditionsLiteracyRate());
			data[i].setLicenseNumberLiteracyRate(dto.getLiteracyRateDtoList().get(i).getLicenseNumberLiteracyRate());
			data[i].setMotorcycleLicenseLiteracyRate(dto.getLiteracyRateDtoList().get(i).getMotorcycleLicenseLiteracyRate());
			data[i].setOthersLiteracyRate(dto.getLiteracyRateDtoList().get(i).getOthersLiteracyRate());
			data[i].setCommercialLicenseLiteracyRate(dto.getLiteracyRateDtoList().get(i).getCommercialLicenseLiteracyRate());
			data[i].setCategorysLiteracyRate(dto.getLiteracyRateDtoList().get(i).getCategorysLiteracyRate());
			data[i].setPublicSafetyCommissionLiteracyRate(dto.getLiteracyRateDtoList().get(i).getPublicSafetyCommissionLiteracyRate());
			data[i].setTotalLiteracyRate(dto.getLiteracyRateDtoList().get(i).getTotalLiteracyRate());


			data[i].setRatioMatch(dto.getRatioMatch());

			result.add(data[i]);
			i++;

		}
		return result;
	}

	public String researchMacth(String correct, String inspection) {
		String researchMacth = "";
		if (correct.equals(inspection)) {
			researchMacth = "一致";
		} else {
			researchMacth = "不一致";
		}
		return researchMacth;
	}


}
