package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.dto.RecognitionResultDto;
import com.example.demo.dto.StatusDataDto;
import com.example.demo.model.AuthenticationNGModel;

/**
 * 認証結果Dao
 * @author naomichi nakajima
 *
 */
public class AuthenticationNGDao {
	/**
	 * pdfで出力するデータを設定するメソッド
	 * @return List<GrandTotalProductModel>
	 */
	public List<AuthenticationNGModel> findByAll(RecognitionResultDto dto) {
		List<AuthenticationNGModel> result = new ArrayList<>();

		List<String> NGImagePathList = new ArrayList<>();
		int NGCount = 0;
		for(StatusDataDto statusDataDto:dto.getStatusDataDtoList()) {
			if (statusDataDto.getStatus().equals("NG")) {
				NGImagePathList.add(statusDataDto.getImagePath());
				NGCount++;
			}
		}

		AuthenticationNGModel[] data = new AuthenticationNGModel[NGCount];

		for (int i = 0; i < NGCount; i++) {
			data[i]=new AuthenticationNGModel();
			data[i].setImagePath(NGImagePathList.get(i));
			result.add(data[i]);
		}

		return result;

	}
}
