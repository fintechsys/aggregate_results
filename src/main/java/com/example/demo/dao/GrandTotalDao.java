package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.model.GrandTotalModel;

/**
 * 総計Dao
 * @author naomichi nakajima
 *
 */
public class GrandTotalDao {
	/**
	 * pdfで出力するデータを設定するメソッド
	 * @return List<GrandTotalProductModel>
	 */
	public List<GrandTotalModel> findByAll() {
		List<GrandTotalModel> result = new ArrayList<>();

		GrandTotalModel data1 = new GrandTotalModel();

		result.add(data1);

		return result;

	}
}
