package com.example.demo.model;

import java.math.BigDecimal;

/**
 * 集計一覧Model
 * @author naomichi nakajima
 *
 */
public class AggregateListModel {

	private String imagePath;

	private String inspectionFullName;

	private String inspectionBirthday;

	private String inspectionAddress;

	private String inspectionGrantDate;

	private String inspectionGrantNumber;

	private String inspectionExpirationDate;

	private String inspectionLicenseColor;

	private String inspectionConditions;

	private String inspectionLicenseNumber;

	private String inspectionMotorcycleLicense;

	private String inspectionOthers;

	private String inspectionCommercialLicense;

	private String inspectionCategorys;

	private String inspectionPublicSafetyCommission;

	private String angleId;

	private String angle;

	private String personalId;

	private String correctFullName;

	private String correctBirthday;

	private String correctAddress;

	private String correctGrantDate;

	private String correctGrantNumber;

	private String correctExpirationDate;

	private String correctLicenseColor;

	private String correctConditions;

	private String correctLicenseNumber;

	private String correctMotorcycleLicense;

	private String correctOthers;

	private String correctCommercialLicense;

	private String correctCategorys;

	private String correctPublicSafetyCommission;

	private BigDecimal ratioMatch;

	private Integer correctFullNameLength;

	private Integer correctBirthdayLength;

	private Integer correctAddressLength;

	private Integer correctGrantDateLength;

	private Integer correctGrantNumberLength;

	private Integer correctExpirationDateLength;

	private Integer correctLicenseColorLength;

	private Integer correctConditionsLength;

	private Integer correctLicenseNumberLength;

	private Integer correctMotorcycleLicenseLength;

	private Integer correctOthersLength;

	private Integer correctCommercialLicenseLength;

	private Integer correctCategorysLength;

	private Integer correctPublicSafetyCommissionLength;

	private Integer correctAnswerFullNameLength;

	private Integer correctAnswerBirthdayLength;

	private Integer correctAnswerAddressLength;

	private Integer correctAnswerGrantDateLength;

	private Integer correctAnswerGrantNumberLength;

	private Integer correctAnswerExpirationDateLength;

	private Integer correctAnswerLicenseColorLength;

	private Integer correctAnswerConditionsLength;

	private Integer correctAnswerLicenseNumberLength;

	private Integer correctAnswerMotorcycleLicenseLength;

	private Integer correctAnswerOthersLength;

	private Integer correctAnswerCommercialLicenseLength;

	private Integer correctAnswerCategorysLength;

	private Integer correctAnswerPublicSafetyCommissionLength;

	private Integer totalCorrectLength;

	private Integer totalCorrectAnswerLength;

	private BigDecimal fullnameLiteracyRate;

	private BigDecimal birthdayLiteracyRate;

	private BigDecimal addressLiteracyRate;

	private BigDecimal grantDateLiteracyRate;

	private BigDecimal grantNumberLiteracyRate;

	private BigDecimal expirationDateLiteracyRate;

	private BigDecimal licenseColorLiteracyRate;

	private BigDecimal conditionsLiteracyRate;

	private BigDecimal licenseNumberLiteracyRate;

	private BigDecimal motorcycleLicenseLiteracyRate;

	private BigDecimal othersLiteracyRate;

	private BigDecimal commercialLicenseLiteracyRate;

	private BigDecimal categorysLiteracyRate;

	private BigDecimal publicSafetyCommissionLiteracyRate;

	private BigDecimal totalLiteracyRate;

	private String researchMacth;

	private String researchMacth1;

	private String researchMacth2;

	private String researchMacth3;

	private String researchMacth4;

	private String researchMacth5;

	private String researchMacth6;

	private String researchMacth7;

	private String researchMacth8;

	private String researchMacth9;

	private String researchMacth10;

	private String researchMacth11;

	private String researchMacth12;

	private String researchMacth13;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getInspectionFullName() {
		return inspectionFullName;
	}

	public void setInspectionFullName(String inspectionFullName) {
		this.inspectionFullName = inspectionFullName;
	}

	public String getInspectionBirthday() {
		return inspectionBirthday;
	}

	public void setInspectionBirthday(String inspectionBirthday) {
		this.inspectionBirthday = inspectionBirthday;
	}

	public String getInspectionAddress() {
		return inspectionAddress;
	}

	public void setInspectionAddress(String inspectionAddress) {
		this.inspectionAddress = inspectionAddress;
	}

	public String getInspectionGrantDate() {
		return inspectionGrantDate;
	}

	public void setInspectionGrantDate(String inspectionGrantDate) {
		this.inspectionGrantDate = inspectionGrantDate;
	}

	public String getInspectionGrantNumber() {
		return inspectionGrantNumber;
	}

	public void setInspectionGrantNumber(String inspectionGrantNumber) {
		this.inspectionGrantNumber = inspectionGrantNumber;
	}

	public String getInspectionExpirationDate() {
		return inspectionExpirationDate;
	}

	public void setInspectionExpirationDate(String inspectionExpirationDate) {
		this.inspectionExpirationDate = inspectionExpirationDate;
	}

	public String getInspectionLicenseColor() {
		return inspectionLicenseColor;
	}

	public void setInspectionLicenseColor(String inspectionLicenseColor) {
		this.inspectionLicenseColor = inspectionLicenseColor;
	}

	public String getInspectionConditions() {
		return inspectionConditions;
	}

	public void setInspectionConditions(String inspectionConditions) {
		this.inspectionConditions = inspectionConditions;
	}

	public String getInspectionLicenseNumber() {
		return inspectionLicenseNumber;
	}

	public void setInspectionLicenseNumber(String inspectionLicenseNumber) {
		this.inspectionLicenseNumber = inspectionLicenseNumber;
	}

	public String getInspectionMotorcycleLicense() {
		return inspectionMotorcycleLicense;
	}

	public void setInspectionMotorcycleLicense(String inspectionMotorcycleLicense) {
		this.inspectionMotorcycleLicense = inspectionMotorcycleLicense;
	}

	public String getInspectionOthers() {
		return inspectionOthers;
	}

	public void setInspectionOthers(String inspectionOthers) {
		this.inspectionOthers = inspectionOthers;
	}

	public String getInspectionCommercialLicense() {
		return inspectionCommercialLicense;
	}

	public void setInspectionCommercialLicense(String inspectionCommercialLicense) {
		this.inspectionCommercialLicense = inspectionCommercialLicense;
	}

	public String getInspectionCategorys() {
		return inspectionCategorys;
	}

	public void setInspectionCategorys(String inspectionCategorys) {
		this.inspectionCategorys = inspectionCategorys;
	}

	public String getInspectionPublicSafetyCommission() {
		return inspectionPublicSafetyCommission;
	}

	public void setInspectionPublicSafetyCommission(String inspectionPublicSafetyCommission) {
		this.inspectionPublicSafetyCommission = inspectionPublicSafetyCommission;
	}

	public String getAngleId() {
		return angleId;
	}

	public void setAngleId(String angleId) {
		this.angleId = angleId;
	}

	public String getAngle() {
		return angle;
	}

	public void setAngle(String angle) {
		this.angle = angle;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getCorrectFullName() {
		return correctFullName;
	}

	public void setCorrectFullName(String correctFullName) {
		this.correctFullName = correctFullName;
	}

	public String getCorrectBirthday() {
		return correctBirthday;
	}

	public void setCorrectBirthday(String correctBirthday) {
		this.correctBirthday = correctBirthday;
	}

	public String getCorrectAddress() {
		return correctAddress;
	}

	public void setCorrectAddress(String correctAddress) {
		this.correctAddress = correctAddress;
	}

	public String getCorrectGrantDate() {
		return correctGrantDate;
	}

	public void setCorrectGrantDate(String correctGrantDate) {
		this.correctGrantDate = correctGrantDate;
	}

	public String getCorrectGrantNumber() {
		return correctGrantNumber;
	}

	public void setCorrectGrantNumber(String correctGrantNumber) {
		this.correctGrantNumber = correctGrantNumber;
	}

	public String getCorrectExpirationDate() {
		return correctExpirationDate;
	}

	public void setCorrectExpirationDate(String correctExpirationDate) {
		this.correctExpirationDate = correctExpirationDate;
	}

	public String getCorrectLicenseColor() {
		return correctLicenseColor;
	}

	public void setCorrectLicenseColor(String correctLicenseColor) {
		this.correctLicenseColor = correctLicenseColor;
	}

	public String getCorrectConditions() {
		return correctConditions;
	}

	public void setCorrectConditions(String correctConditions) {
		this.correctConditions = correctConditions;
	}

	public String getCorrectLicenseNumber() {
		return correctLicenseNumber;
	}

	public void setCorrectLicenseNumber(String correctLicenseNumber) {
		this.correctLicenseNumber = correctLicenseNumber;
	}

	public String getCorrectMotorcycleLicense() {
		return correctMotorcycleLicense;
	}

	public void setCorrectMotorcycleLicense(String correctMotorcycleLicense) {
		this.correctMotorcycleLicense = correctMotorcycleLicense;
	}

	public String getCorrectOthers() {
		return correctOthers;
	}

	public void setCorrectOthers(String correctOthers) {
		this.correctOthers = correctOthers;
	}

	public String getCorrectCommercialLicense() {
		return correctCommercialLicense;
	}

	public void setCorrectCommercialLicense(String correctCommercialLicense) {
		this.correctCommercialLicense = correctCommercialLicense;
	}

	public String getCorrectCategorys() {
		return correctCategorys;
	}

	public void setCorrectCategorys(String correctCategorys) {
		this.correctCategorys = correctCategorys;
	}

	public String getCorrectPublicSafetyCommission() {
		return correctPublicSafetyCommission;
	}

	public void setCorrectPublicSafetyCommission(String correctPublicSafetyCommission) {
		this.correctPublicSafetyCommission = correctPublicSafetyCommission;
	}

	public BigDecimal getRatioMatch() {
		return ratioMatch;
	}

	public void setRatioMatch(BigDecimal ratioMatch) {
		this.ratioMatch = ratioMatch;
	}

	public String getResearchMacth() {
		return researchMacth;
	}

	public void setResearchMacth(String researchMacth) {
		this.researchMacth = researchMacth;
	}

	public String getResearchMacth1() {
		return researchMacth1;
	}

	public void setResearchMacth1(String researchMacth1) {
		this.researchMacth1 = researchMacth1;
	}

	public String getResearchMacth2() {
		return researchMacth2;
	}

	public void setResearchMacth2(String researchMacth2) {
		this.researchMacth2 = researchMacth2;
	}

	public String getResearchMacth3() {
		return researchMacth3;
	}

	public void setResearchMacth3(String researchMacth3) {
		this.researchMacth3 = researchMacth3;
	}

	public String getResearchMacth4() {
		return researchMacth4;
	}

	public void setResearchMacth4(String researchMacth4) {
		this.researchMacth4 = researchMacth4;
	}

	public String getResearchMacth5() {
		return researchMacth5;
	}

	public void setResearchMacth5(String researchMacth5) {
		this.researchMacth5 = researchMacth5;
	}

	public String getResearchMacth6() {
		return researchMacth6;
	}

	public void setResearchMacth6(String researchMacth6) {
		this.researchMacth6 = researchMacth6;
	}

	public String getResearchMacth7() {
		return researchMacth7;
	}

	public void setResearchMacth7(String researchMacth7) {
		this.researchMacth7 = researchMacth7;
	}

	public String getResearchMacth8() {
		return researchMacth8;
	}

	public void setResearchMacth8(String researchMacth8) {
		this.researchMacth8 = researchMacth8;
	}

	public String getResearchMacth9() {
		return researchMacth9;
	}

	public void setResearchMacth9(String researchMacth9) {
		this.researchMacth9 = researchMacth9;
	}

	public String getResearchMacth10() {
		return researchMacth10;
	}

	public void setResearchMacth10(String researchMacth10) {
		this.researchMacth10 = researchMacth10;
	}

	public String getResearchMacth11() {
		return researchMacth11;
	}

	public void setResearchMacth11(String researchMacth11) {
		this.researchMacth11 = researchMacth11;
	}

	public String getResearchMacth12() {
		return researchMacth12;
	}

	public void setResearchMacth12(String researchMacth12) {
		this.researchMacth12 = researchMacth12;
	}

	public String getResearchMacth13() {
		return researchMacth13;
	}

	public void setResearchMacth13(String researchMacth13) {
		this.researchMacth13 = researchMacth13;
	}

	public Integer getCorrectFullNameLength() {
		return correctFullNameLength;
	}

	public void setCorrectFullNameLength(Integer correctFullNameLength) {
		this.correctFullNameLength = correctFullNameLength;
	}

	public Integer getCorrectBirthdayLength() {
		return correctBirthdayLength;
	}

	public void setCorrectBirthdayLength(Integer correctBirthdayLength) {
		this.correctBirthdayLength = correctBirthdayLength;
	}

	public Integer getCorrectAddressLength() {
		return correctAddressLength;
	}

	public void setCorrectAddressLength(Integer correctAddressLength) {
		this.correctAddressLength = correctAddressLength;
	}

	public Integer getCorrectGrantDateLength() {
		return correctGrantDateLength;
	}

	public void setCorrectGrantDateLength(Integer correctGrantDateLength) {
		this.correctGrantDateLength = correctGrantDateLength;
	}

	public Integer getCorrectGrantNumberLength() {
		return correctGrantNumberLength;
	}

	public void setCorrectGrantNumberLength(Integer correctGrantNumberLength) {
		this.correctGrantNumberLength = correctGrantNumberLength;
	}

	public Integer getCorrectExpirationDateLength() {
		return correctExpirationDateLength;
	}

	public void setCorrectExpirationDateLength(Integer correctExpirationDateLength) {
		this.correctExpirationDateLength = correctExpirationDateLength;
	}

	public Integer getCorrectLicenseColorLength() {
		return correctLicenseColorLength;
	}

	public void setCorrectLicenseColorLength(Integer correctLicenseColorLength) {
		this.correctLicenseColorLength = correctLicenseColorLength;
	}

	public Integer getCorrectConditionsLength() {
		return correctConditionsLength;
	}

	public void setCorrectConditionsLength(Integer correctConditionsLength) {
		this.correctConditionsLength = correctConditionsLength;
	}

	public Integer getCorrectLicenseNumberLength() {
		return correctLicenseNumberLength;
	}

	public void setCorrectLicenseNumberLength(Integer correctLicenseNumberLength) {
		this.correctLicenseNumberLength = correctLicenseNumberLength;
	}

	public Integer getCorrectMotorcycleLicenseLength() {
		return correctMotorcycleLicenseLength;
	}

	public void setCorrectMotorcycleLicenseLength(Integer correctMotorcycleLicenseLength) {
		this.correctMotorcycleLicenseLength = correctMotorcycleLicenseLength;
	}

	public Integer getCorrectOthersLength() {
		return correctOthersLength;
	}

	public void setCorrectOthersLength(Integer correctOthersLength) {
		this.correctOthersLength = correctOthersLength;
	}

	public Integer getCorrectCommercialLicenseLength() {
		return correctCommercialLicenseLength;
	}

	public void setCorrectCommercialLicenseLength(Integer correctCommercialLicenseLength) {
		this.correctCommercialLicenseLength = correctCommercialLicenseLength;
	}

	public Integer getCorrectCategorysLength() {
		return correctCategorysLength;
	}

	public void setCorrectCategorysLength(Integer correctCategorysLength) {
		this.correctCategorysLength = correctCategorysLength;
	}

	public Integer getCorrectPublicSafetyCommissionLength() {
		return correctPublicSafetyCommissionLength;
	}

	public void setCorrectPublicSafetyCommissionLength(Integer correctPublicSafetyCommissionLength) {
		this.correctPublicSafetyCommissionLength = correctPublicSafetyCommissionLength;
	}

	public Integer getCorrectAnswerFullNameLength() {
		return correctAnswerFullNameLength;
	}

	public void setCorrectAnswerFullNameLength(Integer correctAnswerFullNameLength) {
		this.correctAnswerFullNameLength = correctAnswerFullNameLength;
	}

	public Integer getCorrectAnswerBirthdayLength() {
		return correctAnswerBirthdayLength;
	}

	public void setCorrectAnswerBirthdayLength(Integer correctAnswerBirthdayLength) {
		this.correctAnswerBirthdayLength = correctAnswerBirthdayLength;
	}

	public Integer getCorrectAnswerAddressLength() {
		return correctAnswerAddressLength;
	}

	public void setCorrectAnswerAddressLength(Integer correctAnswerAddressLength) {
		this.correctAnswerAddressLength = correctAnswerAddressLength;
	}

	public Integer getCorrectAnswerGrantDateLength() {
		return correctAnswerGrantDateLength;
	}

	public void setCorrectAnswerGrantDateLength(Integer correctAnswerGrantDateLength) {
		this.correctAnswerGrantDateLength = correctAnswerGrantDateLength;
	}

	public Integer getCorrectAnswerGrantNumberLength() {
		return correctAnswerGrantNumberLength;
	}

	public void setCorrectAnswerGrantNumberLength(Integer correctAnswerGrantNumberLength) {
		this.correctAnswerGrantNumberLength = correctAnswerGrantNumberLength;
	}

	public Integer getCorrectAnswerExpirationDateLength() {
		return correctAnswerExpirationDateLength;
	}

	public void setCorrectAnswerExpirationDateLength(Integer correctAnswerExpirationDateLength) {
		this.correctAnswerExpirationDateLength = correctAnswerExpirationDateLength;
	}

	public Integer getCorrectAnswerLicenseColorLength() {
		return correctAnswerLicenseColorLength;
	}

	public void setCorrectAnswerLicenseColorLength(Integer correctAnswerLicenseColorLength) {
		this.correctAnswerLicenseColorLength = correctAnswerLicenseColorLength;
	}

	public Integer getCorrectAnswerConditionsLength() {
		return correctAnswerConditionsLength;
	}

	public void setCorrectAnswerConditionsLength(Integer correctAnswerConditionsLength) {
		this.correctAnswerConditionsLength = correctAnswerConditionsLength;
	}

	public Integer getCorrectAnswerLicenseNumberLength() {
		return correctAnswerLicenseNumberLength;
	}

	public void setCorrectAnswerLicenseNumberLength(Integer correctAnswerLicenseNumberLength) {
		this.correctAnswerLicenseNumberLength = correctAnswerLicenseNumberLength;
	}

	public Integer getCorrectAnswerMotorcycleLicenseLength() {
		return correctAnswerMotorcycleLicenseLength;
	}

	public void setCorrectAnswerMotorcycleLicenseLength(Integer correctAnswerMotorcycleLicenseLength) {
		this.correctAnswerMotorcycleLicenseLength = correctAnswerMotorcycleLicenseLength;
	}

	public Integer getCorrectAnswerOthersLength() {
		return correctAnswerOthersLength;
	}

	public void setCorrectAnswerOthersLength(Integer correctAnswerOthersLength) {
		this.correctAnswerOthersLength = correctAnswerOthersLength;
	}

	public Integer getCorrectAnswerCommercialLicenseLength() {
		return correctAnswerCommercialLicenseLength;
	}

	public void setCorrectAnswerCommercialLicenseLength(Integer correctAnswerCommercialLicenseLength) {
		this.correctAnswerCommercialLicenseLength = correctAnswerCommercialLicenseLength;
	}

	public Integer getCorrectAnswerCategorysLength() {
		return correctAnswerCategorysLength;
	}

	public void setCorrectAnswerCategorysLength(Integer correctAnswerCategorysLength) {
		this.correctAnswerCategorysLength = correctAnswerCategorysLength;
	}

	public Integer getCorrectAnswerPublicSafetyCommissionLength() {
		return correctAnswerPublicSafetyCommissionLength;
	}

	public void setCorrectAnswerPublicSafetyCommissionLength(Integer correctAnswerPublicSafetyCommissionLength) {
		this.correctAnswerPublicSafetyCommissionLength = correctAnswerPublicSafetyCommissionLength;
	}

	public Integer getTotalCorrectLength() {
		return totalCorrectLength;
	}

	public void setTotalCorrectLength(Integer totalCorrectLength) {
		this.totalCorrectLength = totalCorrectLength;
	}

	public Integer getTotalCorrectAnswerLength() {
		return totalCorrectAnswerLength;
	}

	public void setTotalCorrectAnswerLength(Integer totalCorrectAnswerLength) {
		this.totalCorrectAnswerLength = totalCorrectAnswerLength;
	}

	public BigDecimal getFullnameLiteracyRate() {
		return fullnameLiteracyRate;
	}

	public void setFullnameLiteracyRate(BigDecimal fullnameLiteracyRate) {
		this.fullnameLiteracyRate = fullnameLiteracyRate;
	}

	public BigDecimal getBirthdayLiteracyRate() {
		return birthdayLiteracyRate;
	}

	public void setBirthdayLiteracyRate(BigDecimal birthdayLiteracyRate) {
		this.birthdayLiteracyRate = birthdayLiteracyRate;
	}

	public BigDecimal getAddressLiteracyRate() {
		return addressLiteracyRate;
	}

	public void setAddressLiteracyRate(BigDecimal addressLiteracyRate) {
		this.addressLiteracyRate = addressLiteracyRate;
	}

	public BigDecimal getGrantDateLiteracyRate() {
		return grantDateLiteracyRate;
	}

	public void setGrantDateLiteracyRate(BigDecimal grantDateLiteracyRate) {
		this.grantDateLiteracyRate = grantDateLiteracyRate;
	}

	public BigDecimal getGrantNumberLiteracyRate() {
		return grantNumberLiteracyRate;
	}

	public void setGrantNumberLiteracyRate(BigDecimal grantNumberLiteracyRate) {
		this.grantNumberLiteracyRate = grantNumberLiteracyRate;
	}

	public BigDecimal getExpirationDateLiteracyRate() {
		return expirationDateLiteracyRate;
	}

	public void setExpirationDateLiteracyRate(BigDecimal expirationDateLiteracyRate) {
		this.expirationDateLiteracyRate = expirationDateLiteracyRate;
	}

	public BigDecimal getLicenseColorLiteracyRate() {
		return licenseColorLiteracyRate;
	}

	public void setLicenseColorLiteracyRate(BigDecimal licenseColorLiteracyRate) {
		this.licenseColorLiteracyRate = licenseColorLiteracyRate;
	}

	public BigDecimal getConditionsLiteracyRate() {
		return conditionsLiteracyRate;
	}

	public void setConditionsLiteracyRate(BigDecimal conditionsLiteracyRate) {
		this.conditionsLiteracyRate = conditionsLiteracyRate;
	}

	public BigDecimal getLicenseNumberLiteracyRate() {
		return licenseNumberLiteracyRate;
	}

	public void setLicenseNumberLiteracyRate(BigDecimal licenseNumberLiteracyRate) {
		this.licenseNumberLiteracyRate = licenseNumberLiteracyRate;
	}

	public BigDecimal getMotorcycleLicenseLiteracyRate() {
		return motorcycleLicenseLiteracyRate;
	}

	public void setMotorcycleLicenseLiteracyRate(BigDecimal motorcycleLicenseLiteracyRate) {
		this.motorcycleLicenseLiteracyRate = motorcycleLicenseLiteracyRate;
	}

	public BigDecimal getOthersLiteracyRate() {
		return othersLiteracyRate;
	}

	public void setOthersLiteracyRate(BigDecimal othersLiteracyRate) {
		this.othersLiteracyRate = othersLiteracyRate;
	}

	public BigDecimal getCommercialLicenseLiteracyRate() {
		return commercialLicenseLiteracyRate;
	}

	public void setCommercialLicenseLiteracyRate(BigDecimal commercialLicenseLiteracyRate) {
		this.commercialLicenseLiteracyRate = commercialLicenseLiteracyRate;
	}

	public BigDecimal getCategorysLiteracyRate() {
		return categorysLiteracyRate;
	}

	public void setCategorysLiteracyRate(BigDecimal categorysLiteracyRate) {
		this.categorysLiteracyRate = categorysLiteracyRate;
	}

	public BigDecimal getPublicSafetyCommissionLiteracyRate() {
		return publicSafetyCommissionLiteracyRate;
	}

	public void setPublicSafetyCommissionLiteracyRate(BigDecimal publicSafetyCommissionLiteracyRate) {
		this.publicSafetyCommissionLiteracyRate = publicSafetyCommissionLiteracyRate;
	}

	public BigDecimal getTotalLiteracyRate() {
		return totalLiteracyRate;
	}

	public void setTotalLiteracyRate(BigDecimal totalLiteracyRate) {
		this.totalLiteracyRate = totalLiteracyRate;
	}

}
