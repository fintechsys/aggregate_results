package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.InspectionResultsEntity;

/**
 * 検証結果データリポジトリ
 *
 * @author naomichi nakajima
 *
 */

@Repository
public interface InspectionResultsRepository extends JpaRepository<InspectionResultsEntity, String> {

	/**
	 * csvファイルをインポートするメソッド
	 * ファイルはここで入力しないとインポートされない
	 */
	@Query(value = " load data local infile :multipartFile "
			+ "into table t_inspection_results "
			+ "fields terminated by ',' "
			+ "optionally enclosed by '\n'"
			+ " LINES TERMINATED BY '\\r\\n' "
			+ "IGNORE 1 LINES", nativeQuery = true)
	@Modifying
	@Transactional
	public void fileUpload(@Param("multipartFile")String multipartFile);

	/**
	 * 検証結果データを全件表示
	 */
	@Query(value = "select * from t_inspection_results", nativeQuery = true)
	public List<InspectionResultsEntity> findByAllInspectionResults();

	/**
	 * 画像パスに紐づいて個人IDを修正
	 * @param personalId
	 * @param imagePath
	 */
	@Modifying
	@Query(value = "update t_inspection_results set personal_id = :personalId where image_path = :imagePath", nativeQuery = true)
	public void updatePersonalId(@Param("personalId") String personalId, @Param("imagePath") String imagePath);

	/**
	 * 画像パスに紐づいてAngleIDを修正
	 * @param angleId
	 * @param imagePath
	 */
	@Modifying
	@Query(value = "update t_inspection_results set angle_id = :angleId where image_path = :imagePath", nativeQuery = true)
	public void updateAngle(@Param("angleId") String angleId, @Param("imagePath") String imagePath);
}
