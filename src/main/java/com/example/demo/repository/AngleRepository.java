package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.AngleEntity;

/**
 * 角度リポジトリ
 * @author nakajima
 *
 */
@Repository
public interface AngleRepository extends JpaRepository<AngleEntity, String> {

	/**
	 * 角度の情報を全件取得
	 * @return
	 */
	@Query(value = "select * from m_angle", nativeQuery = true)
	public List<AngleEntity> findByAllAngle();
}
