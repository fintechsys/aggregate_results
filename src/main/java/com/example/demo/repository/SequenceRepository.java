package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.SequenceEntity;

/**
 * シーケンス番号リポジトリ
 * @author naomichi nakajima
 *
 */
public interface SequenceRepository extends JpaRepository<SequenceEntity, String> {

	/**
	 * シーケンステーブルに個人ID=0とセットする
	 * @return setPersonalId
	 */
	@Query(value = "SELECT setval('personal_id',0) ", nativeQuery = true)
	public String setPersonalId();

	/**
	 * 現在のシーケンス番号を取得する
	 * @return maxPersonalId
	 */
	@Query(value = "SELECT currval('personal_id') ", nativeQuery = true)
	public String nextPersonalId();

	/**
	 * 現在のシーケンス番号を次の番号に更新する
	 * @return 現在のシーケンス番号を更新する
	 */
	@Query(value = "SELECT nextval('personal_id') ", nativeQuery = true)
	public String updatePersonalId();

	@Query(value = "UPDATE sequence SET initial=:initial where name='personal_id'", nativeQuery = true)
	@Modifying
	public void setinitial(@Param("initial") char initial);

	@Query(value = "select initial from sequence where name='personal_id'", nativeQuery = true)
	public char initial();
}
