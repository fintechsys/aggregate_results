package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.InspectionResultsJoinCorrectDataJoinAngleEntity;

/**
 * 検証結果と正解データを結合したフォーム
 * @author nakajima
 *
 */
@Repository
public interface InspectionResultsJoinCorrectDataJoinAngleRepository
		extends JpaRepository<InspectionResultsJoinCorrectDataJoinAngleEntity, String> {

	/**
	 * 検証結果と正解データと角度テーブルを外部結合し全件取得
	 * @return
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id ", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByAllInspectionResultsJoinCorrectData();

	/**
	 * 個人IDが未入力のものだけ検証結果データを取得
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id "
			+ "where I.personal_id = '' or  I.personal_id is null", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByEmptyPrsonalId();

	/**
	 * 検証結果と正解データを内部結合し全件取得
	 * @return
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "inner join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id ", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByAllAggregateList();

	/**
	 * 検証結果と正解データと角度テーブルを外部結合し個人IDを条件として全件取得
	 * @param personalId
	 * @return List<InspectionResultsJoinCorrectJoinAngleDataEntity>
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id "
			+ "where I.personal_id = :personalId", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByPersonalIdForAggregateList(
			@Param("personalId") String personalId);

	/**
	 * 検証結果と正解データと角度テーブルを外部結合し角度IDを条件として全件取得
	 * @param angleId
	 * @return List<InspectionResultsJoinCorrectJoinAngleDataEntity>
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id "
			+ "where I.angle_id = :angleId", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByAllAngleIdForAggregateList(
			@Param("angleId") String angleId);

	/**
	 * 検証結果と正解データと角度テーブルを外部結合し複数の角度IDを条件として全件取得
	 * @param angleId1
	 * @param angleId2
	 * @return List<InspectionResultsJoinCorrectDataJoinAngleEntity>
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id "
			+ "where I.angle_id in(:angleId1,:angleId2)", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByAllTwoAngleIdForAggregateList(
			@Param("angleId1") String angleId1, @Param("angleId2") String angleId2);

	/**
	 * 検証結果と正解データと角度テーブルを外部結合し複数の角度IDを条件として全件取得
	 * @param angleId1
	 * @param angleId2
	 * @param angleId3
	 * @param angleId4
	 * @return  List<InspectionResultsJoinCorrectDataJoinAngleEntity>
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id "
			+ "where I.angle_id in(:angleId1,:angleId2,:angleId3,:angleId4)", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByAllFourAngleIdForAggregateList(
			@Param("angleId1") String angleId1, @Param("angleId2") String angleId2, @Param("angleId3") String angleId3,
			@Param("angleId4") String angleId4);

	/**
	 * 検証結果と正解データと角度テーブルを外部結合し個人IDと角度IDを条件として全件取得
	 * @param personalId
	 * @param angleId
	 * @return List<InspectionResultsJoinCorrectJoinAngleDataEntity>
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id "
			+ "where I.personal_id = :personalId and I.angle_id = :angleId", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByAllPersonalIdAndAngleIdForAggregateLIst(
			@Param("personalId") String personalId, @Param("angleId") String angleId);

	/**
	 *  検証結果と正解データと角度テーブルを外部結合し個人IDと複数の角度IDを条件として全件取得
	 * @param personalId
	 * @param angleId1
	 * @param angleId2
	 * @return List<InspectionResultsJoinCorrectDataJoinAngleEntity>
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id "
			+ "where I.personal_id = :personalId and I.angle_id in(:angleId1,:angleId2)", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByAllPersonalIdAndTwoAngleIdForAggregateList(
			@Param("personalId") String personalId, @Param("angleId1") String angleId1,
			@Param("angleId2") String angleId2);

	/**
	 *  検証結果と正解データと角度テーブルを外部結合し個人IDと複数の角度IDを条件として全件取得
	 * @param personalId
	 * @param angleId1
	 * @param angleId2
	 * @param angleId3
	 * @param angleId4
	 * @return List<InspectionResultsJoinCorrectDataJoinAngleEntity>
	 */
	@Query(value = "select "
			+ "I.image_path,I.full_name i_full_name,"
			+ "I.birthday i_birthday,I.address i_address,"
			+ "I.grant_date i_grant_date,I.grant_number i_grant_number,"
			+ "I.expiration_date i_expiration_date,I.license_color i_license_color,"
			+ "I.conditions i_conditions,I.license_number i_license_number,"
			+ "I.motorcycle_license i_motorcycle_license,I.others i_others,"
			+ "I.commercial_license i_commercial_license,I.categorys i_categorys,"
			+ "I.public_safety_commission i_public_safety_commission,"
			+ "I.personal_id,I.angle_id,A.angle,C.full_name c_full_name,"
			+ "C.birthday c_birthday,C.address c_address,"
			+ "C.grant_date c_grant_date,C.grant_number c_grant_number,"
			+ "C.expiration_date c_expiration_date,C.license_color c_license_color,"
			+ "C.conditions c_conditions,C.license_number c_license_number,"
			+ "C.motorcycle_license c_motorcycle_license,C.others c_others,"
			+ "C.commercial_license c_commercial_license,C.categorys c_categorys,"
			+ "C.public_safety_commission c_public_safety_commission "
			+ "from t_inspection_results I "
			+ "left outer join m_correct_data C "
			+ "on I.personal_id = C.personal_id "
			+ "left outer join m_angle A "
			+ "on I.angle_id = A.angle_id "
			+ "where I.personal_id = :personalId and I.angle_id in(:angleId1,:angleId2,:angleId3,:angleId4)", nativeQuery = true)
	public List<InspectionResultsJoinCorrectDataJoinAngleEntity> findByAllPersonalIdAndFourAngleIdForAggregateList(
			@Param("personalId") String personalId, @Param("angleId1") String angleId1,
			@Param("angleId2") String angleId2, @Param("angleId3") String angleId3, @Param("angleId4") String angleId4);

}
