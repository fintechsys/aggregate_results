package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.CorrectDataEntity;

/**
 * 正解データ登録確認のリポジトリ
 * @author tujii
 *
 */
@Repository
public interface CorrectDataRepository extends JpaRepository<CorrectDataEntity, String> {

	/**
	 * 正解データをDBに登録するメソッド
	 * @param personalId
	 * @param fullName
	 * @param birthday
	 * @param address
	 * @param grantDate
	 * @param grantNumber
	 * @param expirationDate
	 * @param licenseColor
	 * @param conditions
	 * @param licenseNumber
	 * @param motorcycleLicense
	 * @param others
	 * @param commercialLicense
	 * @param catgorys
	 * @param publicSafetyCommission
	 */
	@Query(value = "INSERT INTO m_correct_data "
			+ "VALUES(:personalId,"
			+ ":fullName,"
			+ ":birthday,"
			+ ":address,"
			+ ":grantDate,"
			+ ":grantNumber,"
			+ ":expirationDate,"
			+ ":licenseColor,"
			+ ":conditions,"
			+ ":licenseNumber,"
			+ ":motorcycleLicense,"
			+ ":others,"
			+ ":commercialLicense,"
			+ ":categorys,"
			+ ":publicSafetyCommission,"
			+ "'0')", nativeQuery = true)
	@Modifying
	public void insertCorrectData(
			@Param("personalId") String personalId,
			@Param("fullName") String fullName,
			@Param("birthday") String birthday,
			@Param("address") String address,
			@Param("grantDate") String grantDate,
			@Param("grantNumber") String grantNumber,
			@Param("expirationDate") String expirationDate,
			@Param("licenseColor") String licenseColor,
			@Param("conditions") String conditions,
			@Param("licenseNumber") String licenseNumber,
			@Param("motorcycleLicense") String motorcycleLicense,
			@Param("others") String others,
			@Param("commercialLicense") String commercialLicense,
			@Param("categorys") String catgorys,
			@Param("publicSafetyCommission") String publicSafetyCommission);

	/**
	 * 個人IDを条件に正解データを変更する
	 * @param personalId
	 * @param fullName
	 * @param birthday
	 * @param address
	 * @param grantDate
	 * @param grantNumber
	 * @param expirationDate
	 * @param licenseColor
	 * @param conditions
	 * @param licenseNumber
	 * @param motorcycleLicense
	 * @param others
	 * @param commercialLicense
	 * @param catgorys
	 * @param publicSafetyCommission
	 */
	@Query(value = "UPDATE m_correct_data "
			+ "SET personal_id = :personalId,"
			+ "full_name=:fullName,"
			+ "birthday=:birthday,"
			+ "address=:address,"
			+ "grant_date=:grantDate,"
			+ "grant_number=:grantNumber,"
			+ "expiration_date=:expirationDate,"
			+ "license_color=:licenseColor,"
			+ "conditions=:conditions,"
			+ "license_number=:licenseNumber,"
			+ "motorcycle_license=:motorcycleLicense,"
			+ "others=:others,"
			+ "commercial_license=:commercialLicense,"
			+ "categorys=:categorys,"
			+ "public_safety_commission=:publicSafetyCommission "
			+ "WHERE personal_id = :personalId", nativeQuery = true)
	@Modifying
	public void changeCorrectData(
			@Param("personalId") String personalId,
			@Param("fullName") String fullName,
			@Param("birthday") String birthday,
			@Param("address") String address,
			@Param("grantDate") String grantDate,
			@Param("grantNumber") String grantNumber,
			@Param("expirationDate") String expirationDate,
			@Param("licenseColor") String licenseColor,
			@Param("conditions") String conditions,
			@Param("licenseNumber") String licenseNumber,
			@Param("motorcycleLicense") String motorcycleLicense,
			@Param("others") String others,
			@Param("commercialLicense") String commercialLicense,
			@Param("categorys") String catgorys,
			@Param("publicSafetyCommission") String publicSafetyCommission);

	/**
	 * DBから正解データ全件を取得するメソッド
	 * @return 正解データ全件
	 */
	@Query(value = "SELECT * FROM m_correct_data WHERE delete_flag='0'", nativeQuery = true)
	public List<CorrectDataEntity> findByAllCorrectDataList();

	/**
	 *選択された個人IDを条件にDBから正解データ全件を取得するメソッド
	 * @param personalId
	 * @return 個人IDを条件とした正解データ全件
	 */
	@Query(value = "SELECT * FROM m_correct_data WHERE personal_id=:personalId AND delete_flag='0'", nativeQuery = true)
	public List<CorrectDataEntity> findByPersonalIdCorrectDataList(@Param("personalId") String personalId);

	/**
	 * 選択された個人IDを条件にDBから正解データをレコードで取得するメソッド
	 * @param personalId
	 * @return 個人IDを条件とした正解データのレコード
	 */
	@Query(value = "SELECT * FROM m_correct_data WHERE personal_id=:personalId AND delete_flag='0'", nativeQuery = true)
	public CorrectDataEntity findByPersonalIdCorrectData(@Param("personalId") String personalId);

	/**
	 * 個人IDが存在するか確認するメソッド
	 * @param personalId
	 * @return personal_id
	 */
	@Query(value = "SELECT personal_id FROM m_correct_data WHERE personal_id=:personalId", nativeQuery = true)
	public String checkDuplicatePersonalId(@Param("personalId") String personalId);

	/**
	 * 正解データを論理削除
	 * @param personalId
	 */
	@Query(value = "UPDATE m_correct_data SET delete_flag = '01' WHERE personal_id = :personalId", nativeQuery = true)
	@Modifying
	public void deleteCorrectData(@Param("personalId") String personalId);



}
