package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.StatusDataEntity;

/**
 * 検証状態データレポジトリ
 * @author naomichi nakajima
 *
 */
@Repository
public interface StatusDataRepository extends JpaRepository<StatusDataEntity, String> {

	/**
	 * csvファイルをインポートするメソッド
	 * ファイルはここで入力しないとインポートされない
	 */
	@Query(value = " load data local infile :multipartFile "
			+ "into table t_status_data "
			+ "fields terminated by ',' "
			+ "optionally enclosed by '\n'"
			+ " LINES TERMINATED BY '\\r\\n' "
			+ "IGNORE 1 LINES", nativeQuery = true)
	@Modifying
	@Transactional
	public void fileUpload(@Param("multipartFile")String multipartFile);

	/**
	 * 検証状態データを全件取得
	 * @return List<StatusDataEntity>
	 */
	@Query(value = "select * from t_status_data", nativeQuery = true)
	public List<StatusDataEntity> findByAllStatusData();
}
