package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 検証状態データエンティティ
 * @author naomichi nakajima
 *
 */
@Entity
@Table(name = "t_status_data")
public class StatusDataEntity {

	@Id
	@Column(name = "image_path")
	private String imagePath;

	@Column(name = "status")
	private String status;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
