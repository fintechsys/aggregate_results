package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * シーケンス番号エンティティ
 * @author naomichi nakajima
 *
 */
@Entity
@Table(name = "sequence")
public class SequenceEntity {

	@Id
	@Column(name="name")
	private String name;

	@Column(name="initial")
	private String initial;

	@Column(name="current_value")
	private Integer currentValue;

	@Column(name="increment")
	private Integer increment;

}
