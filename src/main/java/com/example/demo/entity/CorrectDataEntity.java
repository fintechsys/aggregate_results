package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 正解データエンティティ
 * @author tujii
 *
 */
@Entity
@Table(name = "m_correct_data")
public class CorrectDataEntity {

	@Id
	@Column(name = "personal_id")
	private String personalId;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "birthday")
	private String birthday;

	@Column(name = "address")
	private String address;

	@Column(name = "grant_date")
	private String grantDate;

	@Column(name = "grant_number")
	private String grantNumber;

	@Column(name = "expiration_date")
	private String expirationDate;

	@Column(name = "license_color")
	private String licenseColor;

	@Column(name = "conditions")
	private String conditions;

	@Column(name = "license_number")
	private String licenseNumber;

	@Column(name = "motorcycle_license")
	private String motorcycleLicense;

	@Column(name = "others")
	private String others;

	@Column(name = "commercial_license")
	private String commercialLicense;

	@Column(name = "categorys")
	private String categorys;

	@Column(name = "public_safety_commission")
	private String publicSafetyCommission;

	@Column(name = "delete_flag")
	private String deleteFlag;

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGrantDate() {
		return grantDate;
	}

	public void setGrantDate(String grantDate) {
		this.grantDate = grantDate;
	}

	public String getGrantNumber() {
		return grantNumber;
	}

	public void setGrantNumber(String grantNumber) {
		this.grantNumber = grantNumber;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getLicenseColor() {
		return licenseColor;
	}

	public void setLicenseColor(String licenseColor) {
		this.licenseColor = licenseColor;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getMotorcycleLicense() {
		return motorcycleLicense;
	}

	public void setMotorcycleLicense(String motorcycleLicense) {
		this.motorcycleLicense = motorcycleLicense;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getCommercialLicense() {
		return commercialLicense;
	}

	public void setCommercialLicense(String commercialLicense) {
		this.commercialLicense = commercialLicense;
	}

	public String getCategorys() {
		return categorys;
	}

	public void setCategorys(String categorys) {
		this.categorys = categorys;
	}

	public String getPublicSafetyCommission() {
		return publicSafetyCommission;
	}

	public void setPublicSafetyCommission(String publicSafetyCommission) {
		this.publicSafetyCommission = publicSafetyCommission;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

}
