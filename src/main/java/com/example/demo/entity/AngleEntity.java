package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 角度エンティティ
 * @author naomichi nakajima
 *
 */
@Entity
@Table(name = "m_angle")
public class AngleEntity {

	@Id
	@Column(name = "angle_id")
	private String angleId;

	@Column(name = "angle")
	private String angle;

	public String getAngleId() {
		return angleId;
	}

	public void setAngleId(String angleId) {
		this.angleId = angleId;
	}

	public String getAngle() {
		return angle;
	}

	public void setAngle(String angle) {
		this.angle = angle;
	}
}
