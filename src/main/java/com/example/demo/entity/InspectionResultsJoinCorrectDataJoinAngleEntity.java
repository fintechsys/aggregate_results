package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 検証結果、正解データ結合エンティティ
 *
 * @author nakajima
 */

@Entity
public class InspectionResultsJoinCorrectDataJoinAngleEntity {

	@Id
	@Column(name = "image_path")
	private String imagePath;

	@Column(name = "i_full_name")
	private String inspectionFullName;

	@Column(name = "i_birthday")
	private String inspectionBirthday;

	@Column(name = "i_address")
	private String inspectionAddress;

	@Column(name = "i_grant_date")
	private String inspectionGrantDate;

	@Column(name = "i_grant_number")
	private String inspectionGrantNumber;

	@Column(name = "i_expiration_date")
	private String inspectionExpirationDate;

	@Column(name = "i_license_color")
	private String inspectionLicenseColor;

	@Column(name = "i_conditions")
	private String inspectionConditions;

	@Column(name = "i_license_number")
	private String inspectionLicenseNumber;

	@Column(name = "i_motorcycle_license")
	private String inspectionMotorcycleLicense;

	@Column(name = "i_others")
	private String inspectionOthers;

	@Column(name = "i_commercial_license")
	private String inspectionCommercialLicense;

	@Column(name = "i_categorys")
	private String inspectionCategorys;

	@Column(name = "i_public_safety_commission")
	private String inspectionPublicSafetyCommission;

	@Column(name = "personal_id")
	private String personalId;

	@Column(name = "angle_id")
	private String angleId;

	@Column(name = "angle")
	private String angle;

	@Column(name = "c_full_name")
	private String correctFullName;

	@Column(name = "c_birthday")
	private String correctBirthday;

	@Column(name = "c_address")
	private String correctAddress;

	@Column(name = "c_grant_date")
	private String correctGrantDate;

	@Column(name = "c_grant_number")
	private String correctGrantNumber;

	@Column(name = "c_expiration_date")
	private String correctExpirationDate;

	@Column(name = "c_license_color")
	private String correctLicenseColor;

	@Column(name = "c_conditions")
	private String correctConditions;

	@Column(name = "c_license_number")
	private String correctLicenseNumber;

	@Column(name = "c_motorcycle_license")
	private String correctMotorcycleLicense;

	@Column(name = "c_others")
	private String correctOthers;

	@Column(name = "c_commercial_license")
	private String correctCommercialLicense;

	@Column(name = "c_categorys")
	private String correctCategorys;

	@Column(name = "c_public_safety_commission")
	private String correctPublicSafetyCommission;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getInspectionFullName() {
		return inspectionFullName;
	}

	public void setInspectionFullName(String inspectionFullName) {
		this.inspectionFullName = inspectionFullName;
	}

	public String getInspectionBirthday() {
		return inspectionBirthday;
	}

	public void setInspectionBirthday(String inspectionBirthday) {
		this.inspectionBirthday = inspectionBirthday;
	}

	public String getInspectionAddress() {
		return inspectionAddress;
	}

	public void setInspectionAddress(String inspectionAddress) {
		this.inspectionAddress = inspectionAddress;
	}

	public String getInspectionGrantDate() {
		return inspectionGrantDate;
	}

	public void setInspectionGrantDate(String inspectionGrantDate) {
		this.inspectionGrantDate = inspectionGrantDate;
	}

	public String getInspectionGrantNumber() {
		return inspectionGrantNumber;
	}

	public void setInspectionGrantNumber(String inspectionGrantNumber) {
		this.inspectionGrantNumber = inspectionGrantNumber;
	}

	public String getInspectionExpirationDate() {
		return inspectionExpirationDate;
	}

	public void setInspectionExpirationDate(String inspectionExpirationDate) {
		this.inspectionExpirationDate = inspectionExpirationDate;
	}

	public String getInspectionLicenseColor() {
		return inspectionLicenseColor;
	}

	public void setInspectionLicenseColor(String inspectionLicenseColor) {
		this.inspectionLicenseColor = inspectionLicenseColor;
	}

	public String getInspectionConditions() {
		return inspectionConditions;
	}

	public void setInspectionConditions(String inspectionConditions) {
		this.inspectionConditions = inspectionConditions;
	}

	public String getInspectionLicenseNumber() {
		return inspectionLicenseNumber;
	}

	public void setInspectionLicenseNumber(String inspectionLicenseNumber) {
		this.inspectionLicenseNumber = inspectionLicenseNumber;
	}

	public String getInspectionMotorcycleLicense() {
		return inspectionMotorcycleLicense;
	}

	public void setInspectionMotorcycleLicense(String inspectionMotorcycleLicense) {
		this.inspectionMotorcycleLicense = inspectionMotorcycleLicense;
	}

	public String getInspectionOthers() {
		return inspectionOthers;
	}

	public void setInspectionOthers(String inspectionOthers) {
		this.inspectionOthers = inspectionOthers;
	}

	public String getInspectionCommercialLicense() {
		return inspectionCommercialLicense;
	}

	public void setInspectionCommercialLicense(String inspectionCommercialLicense) {
		this.inspectionCommercialLicense = inspectionCommercialLicense;
	}

	public String getInspectionCategorys() {
		return inspectionCategorys;
	}

	public void setInspectionCategorys(String inspectionCategorys) {
		this.inspectionCategorys = inspectionCategorys;
	}

	public String getInspectionPublicSafetyCommission() {
		return inspectionPublicSafetyCommission;
	}

	public void setInspectionPublicSafetyCommission(String inspectionPublicSafetyCommission) {
		this.inspectionPublicSafetyCommission = inspectionPublicSafetyCommission;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getAngleId() {
		return angleId;
	}

	public void setAngleId(String angleId) {
		this.angleId = angleId;
	}

	public String getAngle() {
		return angle;
	}

	public void setAngle(String angle) {
		this.angle = angle;
	}

	public String getCorrectFullName() {
		return correctFullName;
	}

	public void setCorrectFullName(String correctFullName) {
		this.correctFullName = correctFullName;
	}

	public String getCorrectBirthday() {
		return correctBirthday;
	}

	public void setCorrectBirthday(String correctBirthday) {
		this.correctBirthday = correctBirthday;
	}

	public String getCorrectAddress() {
		return correctAddress;
	}

	public void setCorrectAddress(String correctAddress) {
		this.correctAddress = correctAddress;
	}

	public String getCorrectGrantDate() {
		return correctGrantDate;
	}

	public void setCorrectGrantDate(String correctGrantDate) {
		this.correctGrantDate = correctGrantDate;
	}

	public String getCorrectGrantNumber() {
		return correctGrantNumber;
	}

	public void setCorrectGrantNumber(String correctGrantNumber) {
		this.correctGrantNumber = correctGrantNumber;
	}

	public String getCorrectExpirationDate() {
		return correctExpirationDate;
	}

	public void setCorrectExpirationDate(String correctExpirationDate) {
		this.correctExpirationDate = correctExpirationDate;
	}

	public String getCorrectLicenseColor() {
		return correctLicenseColor;
	}

	public void setCorrectLicenseColor(String correctLicenseColor) {
		this.correctLicenseColor = correctLicenseColor;
	}

	public String getCorrectConditions() {
		return correctConditions;
	}

	public void setCorrectConditions(String correctConditions) {
		this.correctConditions = correctConditions;
	}

	public String getCorrectLicenseNumber() {
		return correctLicenseNumber;
	}

	public void setCorrectLicenseNumber(String correctLicenseNumber) {
		this.correctLicenseNumber = correctLicenseNumber;
	}

	public String getCorrectMotorcycleLicense() {
		return correctMotorcycleLicense;
	}

	public void setCorrectMotorcycleLicense(String correctMotorcycleLicense) {
		this.correctMotorcycleLicense = correctMotorcycleLicense;
	}

	public String getCorrectOthers() {
		return correctOthers;
	}

	public void setCorrectOthers(String correctOthers) {
		this.correctOthers = correctOthers;
	}

	public String getCorrectCommercialLicense() {
		return correctCommercialLicense;
	}

	public void setCorrectCommercialLicense(String correctCommercialLicense) {
		this.correctCommercialLicense = correctCommercialLicense;
	}

	public String getCorrectCategorys() {
		return correctCategorys;
	}

	public void setCorrectCategorys(String correctCategorys) {
		this.correctCategorys = correctCategorys;
	}

	public String getCorrectPublicSafetyCommission() {
		return correctPublicSafetyCommission;
	}

	public void setCorrectPublicSafetyCommission(String correctPublicSafetyCommission) {
		this.correctPublicSafetyCommission = correctPublicSafetyCommission;
	}

}