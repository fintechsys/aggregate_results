package com.example.demo.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * 認証結果Dto
 * @author nakajima
 *
 */
public class RecognitionResultDto {

	private List<StatusDataDto> statusDataDtoList;

	private Integer recognitionResultCount;

	private BigDecimal recognitionResultRatio;

	public List<StatusDataDto> getStatusDataDtoList() {
		return statusDataDtoList;
	}

	public void setStatusDataDtoList(List<StatusDataDto> statusDataDtoList) {
		this.statusDataDtoList = statusDataDtoList;
	}

	public Integer getRecognitionResultCount() {
		return recognitionResultCount;
	}

	public void setRecognitionResultCount(Integer recognitionResultCount) {
		this.recognitionResultCount = recognitionResultCount;
	}

	public BigDecimal getRecognitionResultRatio() {
		return recognitionResultRatio;
	}

	public void setRecognitionResultRatio(BigDecimal recognitionResultRatio) {
		this.recognitionResultRatio = recognitionResultRatio;
	}



}
