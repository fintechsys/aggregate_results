package com.example.demo.dto;

/**
 * 検証状態データのDto
 * @author naomichi nakajima
 *
 */
public class StatusDataDto {

	private String imagePath;

	private String status;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
