package com.example.demo.dto;

import java.math.BigDecimal;

/**
 * 識字率Dto
 * @author naomichi nakajima
 *
 */
public class LiteracyRateDto {

	private Integer correctFullNameLength;

	private Integer correctBirthdayLength;

	private Integer correctAddressLength;

	private Integer correctGrantDateLength;

	private Integer correctGrantNumberLength;

	private Integer correctExpirationDateLength;

	private Integer correctLicenseColorLength;

	private Integer correctConditionsLength;

	private Integer correctLicenseNumberLength;

	private Integer correctMotorcycleLicenseLength;

	private Integer correctOthersLength;

	private Integer correctCommercialLicenseLength;

	private Integer correctCategorysLength;

	private Integer correctPublicSafetyCommissionLength;

	private Integer correctAnswerFullNameLength;

	private Integer correctAnswerBirthdayLength;

	private Integer correctAnswerAddressLength;

	private Integer correctAnswerGrantDateLength;

	private Integer correctAnswerGrantNumberLength;

	private Integer correctAnswerExpirationDateLength;

	private Integer correctAnswerLicenseColorLength;

	private Integer correctAnswerConditionsLength;

	private Integer correctAnswerLicenseNumberLength;

	private Integer correctAnswerMotorcycleLicenseLength;

	private Integer correctAnswerOthersLength;

	private Integer correctAnswerCommercialLicenseLength;

	private Integer correctAnswerCategorysLength;

	private Integer correctAnswerPublicSafetyCommissionLength;

	private Integer totalCorrectLength;

	private Integer totalCorrectAnswerLength;

	private BigDecimal fullnameLiteracyRate;

	private BigDecimal birthdayLiteracyRate;

	private BigDecimal addressLiteracyRate;

	private BigDecimal grantDateLiteracyRate;

	private BigDecimal grantNumberLiteracyRate;

	private BigDecimal expirationDateLiteracyRate;

	private BigDecimal licenseColorLiteracyRate;

	private BigDecimal conditionsLiteracyRate;

	private BigDecimal licenseNumberLiteracyRate;

	private BigDecimal motorcycleLicenseLiteracyRate;

	private BigDecimal othersLiteracyRate;

	private BigDecimal commercialLicenseLiteracyRate;

	private BigDecimal categorysLiteracyRate;

	private BigDecimal publicSafetyCommissionLiteracyRate;

	private BigDecimal totalLiteracyRate;

	public Integer getCorrectFullNameLength() {
		return correctFullNameLength;
	}

	public void setCorrectFullNameLength(Integer correctFullNameLength) {
		this.correctFullNameLength = correctFullNameLength;
	}

	public Integer getCorrectBirthdayLength() {
		return correctBirthdayLength;
	}

	public void setCorrectBirthdayLength(Integer correctBirthdayLength) {
		this.correctBirthdayLength = correctBirthdayLength;
	}

	public Integer getCorrectAddressLength() {
		return correctAddressLength;
	}

	public void setCorrectAddressLength(Integer correctAddressLength) {
		this.correctAddressLength = correctAddressLength;
	}

	public Integer getCorrectGrantDateLength() {
		return correctGrantDateLength;
	}

	public void setCorrectGrantDateLength(Integer correctGrantDateLength) {
		this.correctGrantDateLength = correctGrantDateLength;
	}

	public Integer getCorrectGrantNumberLength() {
		return correctGrantNumberLength;
	}

	public void setCorrectGrantNumberLength(Integer correctGrantNumberLength) {
		this.correctGrantNumberLength = correctGrantNumberLength;
	}

	public Integer getCorrectExpirationDateLength() {
		return correctExpirationDateLength;
	}

	public void setCorrectExpirationDateLength(Integer correctExpirationDateLength) {
		this.correctExpirationDateLength = correctExpirationDateLength;
	}

	public Integer getCorrectLicenseColorLength() {
		return correctLicenseColorLength;
	}

	public void setCorrectLicenseColorLength(Integer correctLicenseColorLength) {
		this.correctLicenseColorLength = correctLicenseColorLength;
	}

	public Integer getCorrectConditionsLength() {
		return correctConditionsLength;
	}

	public void setCorrectConditionsLength(Integer correctConditionsLength) {
		this.correctConditionsLength = correctConditionsLength;
	}

	public Integer getCorrectLicenseNumberLength() {
		return correctLicenseNumberLength;
	}

	public void setCorrectLicenseNumberLength(Integer correctLicenseNumberLength) {
		this.correctLicenseNumberLength = correctLicenseNumberLength;
	}

	public Integer getCorrectMotorcycleLicenseLength() {
		return correctMotorcycleLicenseLength;
	}

	public void setCorrectMotorcycleLicenseLength(Integer correctMotorcycleLicenseLength) {
		this.correctMotorcycleLicenseLength = correctMotorcycleLicenseLength;
	}

	public Integer getCorrectOthersLength() {
		return correctOthersLength;
	}

	public void setCorrectOthersLength(Integer correctOthersLength) {
		this.correctOthersLength = correctOthersLength;
	}

	public Integer getCorrectCommercialLicenseLength() {
		return correctCommercialLicenseLength;
	}

	public void setCorrectCommercialLicenseLength(Integer correctCommercialLicenseLength) {
		this.correctCommercialLicenseLength = correctCommercialLicenseLength;
	}

	public Integer getCorrectCategorysLength() {
		return correctCategorysLength;
	}

	public void setCorrectCategorysLength(Integer correctCategorysLength) {
		this.correctCategorysLength = correctCategorysLength;
	}

	public Integer getCorrectPublicSafetyCommissionLength() {
		return correctPublicSafetyCommissionLength;
	}

	public void setCorrectPublicSafetyCommissionLength(Integer correctPublicSafetyCommissionLength) {
		this.correctPublicSafetyCommissionLength = correctPublicSafetyCommissionLength;
	}

	public Integer getCorrectAnswerFullNameLength() {
		return correctAnswerFullNameLength;
	}

	public void setCorrectAnswerFullNameLength(Integer correctAnswerFullNameLength) {
		this.correctAnswerFullNameLength = correctAnswerFullNameLength;
	}

	public Integer getCorrectAnswerBirthdayLength() {
		return correctAnswerBirthdayLength;
	}

	public void setCorrectAnswerBirthdayLength(Integer correctAnswerBirthdayLength) {
		this.correctAnswerBirthdayLength = correctAnswerBirthdayLength;
	}

	public Integer getCorrectAnswerAddressLength() {
		return correctAnswerAddressLength;
	}

	public void setCorrectAnswerAddressLength(Integer correctAnswerAddressLength) {
		this.correctAnswerAddressLength = correctAnswerAddressLength;
	}

	public Integer getCorrectAnswerGrantDateLength() {
		return correctAnswerGrantDateLength;
	}

	public void setCorrectAnswerGrantDateLength(Integer correctAnswerGrantDateLength) {
		this.correctAnswerGrantDateLength = correctAnswerGrantDateLength;
	}

	public Integer getCorrectAnswerGrantNumberLength() {
		return correctAnswerGrantNumberLength;
	}

	public void setCorrectAnswerGrantNumberLength(Integer correctAnswerGrantNumberLength) {
		this.correctAnswerGrantNumberLength = correctAnswerGrantNumberLength;
	}

	public Integer getCorrectAnswerExpirationDateLength() {
		return correctAnswerExpirationDateLength;
	}

	public void setCorrectAnswerExpirationDateLength(Integer correctAnswerExpirationDateLength) {
		this.correctAnswerExpirationDateLength = correctAnswerExpirationDateLength;
	}

	public Integer getCorrectAnswerLicenseColorLength() {
		return correctAnswerLicenseColorLength;
	}

	public void setCorrectAnswerLicenseColorLength(Integer correctAnswerLicenseColorLength) {
		this.correctAnswerLicenseColorLength = correctAnswerLicenseColorLength;
	}

	public Integer getCorrectAnswerConditionsLength() {
		return correctAnswerConditionsLength;
	}

	public void setCorrectAnswerConditionsLength(Integer correctAnswerConditionsLength) {
		this.correctAnswerConditionsLength = correctAnswerConditionsLength;
	}

	public Integer getCorrectAnswerLicenseNumberLength() {
		return correctAnswerLicenseNumberLength;
	}

	public void setCorrectAnswerLicenseNumberLength(Integer correctAnswerLicenseNumberLength) {
		this.correctAnswerLicenseNumberLength = correctAnswerLicenseNumberLength;
	}

	public Integer getCorrectAnswerMotorcycleLicenseLength() {
		return correctAnswerMotorcycleLicenseLength;
	}

	public void setCorrectAnswerMotorcycleLicenseLength(Integer correctAnswerMotorcycleLicenseLength) {
		this.correctAnswerMotorcycleLicenseLength = correctAnswerMotorcycleLicenseLength;
	}

	public Integer getCorrectAnswerOthersLength() {
		return correctAnswerOthersLength;
	}

	public void setCorrectAnswerOthersLength(Integer correctAnswerOthersLength) {
		this.correctAnswerOthersLength = correctAnswerOthersLength;
	}

	public Integer getCorrectAnswerCommercialLicenseLength() {
		return correctAnswerCommercialLicenseLength;
	}

	public void setCorrectAnswerCommercialLicenseLength(Integer correctAnswerCommercialLicenseLength) {
		this.correctAnswerCommercialLicenseLength = correctAnswerCommercialLicenseLength;
	}

	public Integer getCorrectAnswerCategorysLength() {
		return correctAnswerCategorysLength;
	}

	public void setCorrectAnswerCategorysLength(Integer correctAnswerCategorysLength) {
		this.correctAnswerCategorysLength = correctAnswerCategorysLength;
	}

	public Integer getCorrectAnswerPublicSafetyCommissionLength() {
		return correctAnswerPublicSafetyCommissionLength;
	}

	public void setCorrectAnswerPublicSafetyCommissionLength(Integer correctAnswerPublicSafetyCommissionLength) {
		this.correctAnswerPublicSafetyCommissionLength = correctAnswerPublicSafetyCommissionLength;
	}

	public Integer getTotalCorrectLength() {
		return totalCorrectLength;
	}

	public void setTotalCorrectLength(Integer totalCorrectLength) {
		this.totalCorrectLength = totalCorrectLength;
	}

	public Integer getTotalCorrectAnswerLength() {
		return totalCorrectAnswerLength;
	}

	public void setTotalCorrectAnswerLength(Integer totalCorrectAnswerLength) {
		this.totalCorrectAnswerLength = totalCorrectAnswerLength;
	}

	public BigDecimal getFullnameLiteracyRate() {
		return fullnameLiteracyRate;
	}

	public void setFullnameLiteracyRate(BigDecimal fullnameLiteracyRate) {
		this.fullnameLiteracyRate = fullnameLiteracyRate;
	}

	public BigDecimal getBirthdayLiteracyRate() {
		return birthdayLiteracyRate;
	}

	public void setBirthdayLiteracyRate(BigDecimal birthdayLiteracyRate) {
		this.birthdayLiteracyRate = birthdayLiteracyRate;
	}

	public BigDecimal getAddressLiteracyRate() {
		return addressLiteracyRate;
	}

	public void setAddressLiteracyRate(BigDecimal addressLiteracyRate) {
		this.addressLiteracyRate = addressLiteracyRate;
	}

	public BigDecimal getGrantDateLiteracyRate() {
		return grantDateLiteracyRate;
	}

	public void setGrantDateLiteracyRate(BigDecimal grantDateLiteracyRate) {
		this.grantDateLiteracyRate = grantDateLiteracyRate;
	}

	public BigDecimal getGrantNumberLiteracyRate() {
		return grantNumberLiteracyRate;
	}

	public void setGrantNumberLiteracyRate(BigDecimal grantNumberLiteracyRate) {
		this.grantNumberLiteracyRate = grantNumberLiteracyRate;
	}

	public BigDecimal getExpirationDateLiteracyRate() {
		return expirationDateLiteracyRate;
	}

	public void setExpirationDateLiteracyRate(BigDecimal expirationDateLiteracyRate) {
		this.expirationDateLiteracyRate = expirationDateLiteracyRate;
	}

	public BigDecimal getLicenseColorLiteracyRate() {
		return licenseColorLiteracyRate;
	}

	public void setLicenseColorLiteracyRate(BigDecimal licenseColorLiteracyRate) {
		this.licenseColorLiteracyRate = licenseColorLiteracyRate;
	}

	public BigDecimal getConditionsLiteracyRate() {
		return conditionsLiteracyRate;
	}

	public void setConditionsLiteracyRate(BigDecimal conditionsLiteracyRate) {
		this.conditionsLiteracyRate = conditionsLiteracyRate;
	}

	public BigDecimal getLicenseNumberLiteracyRate() {
		return licenseNumberLiteracyRate;
	}

	public void setLicenseNumberLiteracyRate(BigDecimal licenseNumberLiteracyRate) {
		this.licenseNumberLiteracyRate = licenseNumberLiteracyRate;
	}

	public BigDecimal getMotorcycleLicenseLiteracyRate() {
		return motorcycleLicenseLiteracyRate;
	}

	public void setMotorcycleLicenseLiteracyRate(BigDecimal motorcycleLicenseLiteracyRate) {
		this.motorcycleLicenseLiteracyRate = motorcycleLicenseLiteracyRate;
	}

	public BigDecimal getOthersLiteracyRate() {
		return othersLiteracyRate;
	}

	public void setOthersLiteracyRate(BigDecimal othersLiteracyRate) {
		this.othersLiteracyRate = othersLiteracyRate;
	}

	public BigDecimal getCommercialLicenseLiteracyRate() {
		return commercialLicenseLiteracyRate;
	}

	public void setCommercialLicenseLiteracyRate(BigDecimal commercialLicenseLiteracyRate) {
		this.commercialLicenseLiteracyRate = commercialLicenseLiteracyRate;
	}

	public BigDecimal getCategorysLiteracyRate() {
		return categorysLiteracyRate;
	}

	public void setCategorysLiteracyRate(BigDecimal categorysLiteracyRate) {
		this.categorysLiteracyRate = categorysLiteracyRate;
	}

	public BigDecimal getPublicSafetyCommissionLiteracyRate() {
		return publicSafetyCommissionLiteracyRate;
	}

	public void setPublicSafetyCommissionLiteracyRate(BigDecimal publicSafetyCommissionLiteracyRate) {
		this.publicSafetyCommissionLiteracyRate = publicSafetyCommissionLiteracyRate;
	}

	public BigDecimal getTotalLiteracyRate() {
		return totalLiteracyRate;
	}

	public void setTotalLiteracyRate(BigDecimal totalLiteracyRate) {
		this.totalLiteracyRate = totalLiteracyRate;
	}

}
