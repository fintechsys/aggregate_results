package com.example.demo.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * 総計Dto
 * @author naomichi nakajima
 *
 */
public class GrandTotalDto {

	private Integer totalMacthRatioCount100;

	private Integer totalMacthRatioCount90;

	private Integer totalMacthRatioCount80;

	private Integer totalMacthRatioCountLess;

	private Integer totalLiteracyRatCount100;

	private Integer totalLiteracyRateCount90;

	private Integer totalLiteracyRateCount80;

	private Integer totalLiteracyRateCountLess;

	private BigDecimal totalMacthRatio;

	private BigDecimal totalLiteracyRate;

	private BigDecimal grandTotalMacthRatio;

	private BigDecimal grandTotalLiteracyRate;

	private List<LiteracyRateDto> literacyRateDtoList;

	public Integer getTotalMacthRatioCount100() {
		return totalMacthRatioCount100;
	}

	public void setTotalMacthRatioCount100(Integer totalMacthRatioCount100) {
		this.totalMacthRatioCount100 = totalMacthRatioCount100;
	}

	public Integer getTotalMacthRatioCount90() {
		return totalMacthRatioCount90;
	}

	public void setTotalMacthRatioCount90(Integer totalMacthRatioCount90) {
		this.totalMacthRatioCount90 = totalMacthRatioCount90;
	}

	public Integer getTotalMacthRatioCount80() {
		return totalMacthRatioCount80;
	}

	public void setTotalMacthRatioCount80(Integer totalMacthRatioCount80) {
		this.totalMacthRatioCount80 = totalMacthRatioCount80;
	}

	public Integer getTotalMacthRatioCountLess() {
		return totalMacthRatioCountLess;
	}

	public void setTotalMacthRatioCountLess(Integer totalMacthRatioCountLess) {
		this.totalMacthRatioCountLess = totalMacthRatioCountLess;
	}

	public Integer getTotalLiteracyRatCount100() {
		return totalLiteracyRatCount100;
	}

	public void setTotalLiteracyRatCount100(Integer totalLiteracyRatCount100) {
		this.totalLiteracyRatCount100 = totalLiteracyRatCount100;
	}

	public Integer getTotalLiteracyRateCount90() {
		return totalLiteracyRateCount90;
	}

	public void setTotalLiteracyRateCount90(Integer totalLiteracyRateCount90) {
		this.totalLiteracyRateCount90 = totalLiteracyRateCount90;
	}

	public Integer getTotalLiteracyRateCount80() {
		return totalLiteracyRateCount80;
	}

	public void setTotalLiteracyRateCount80(Integer totalLiteracyRateCount80) {
		this.totalLiteracyRateCount80 = totalLiteracyRateCount80;
	}

	public Integer getTotalLiteracyRateCountLess() {
		return totalLiteracyRateCountLess;
	}

	public void setTotalLiteracyRateCountLess(Integer totalLiteracyRateCountLess) {
		this.totalLiteracyRateCountLess = totalLiteracyRateCountLess;
	}

	public BigDecimal getTotalMacthRatio() {
		return totalMacthRatio;
	}

	public void setTotalMacthRatio(BigDecimal totalMacthRatio) {
		this.totalMacthRatio = totalMacthRatio;
	}

	public BigDecimal getTotalLiteracyRate() {
		return totalLiteracyRate;
	}

	public void setTotalLiteracyRate(BigDecimal totalLiteracyRate) {
		this.totalLiteracyRate = totalLiteracyRate;
	}

	public BigDecimal getGrandTotalMacthRatio() {
		return grandTotalMacthRatio;
	}

	public void setGrandTotalMacthRatio(BigDecimal grandTotalMacthRatio) {
		this.grandTotalMacthRatio = grandTotalMacthRatio;
	}

	public BigDecimal getGrandTotalLiteracyRate() {
		return grandTotalLiteracyRate;
	}

	public void setGrandTotalLiteracyRate(BigDecimal grandTotalLiteracyRate) {
		this.grandTotalLiteracyRate = grandTotalLiteracyRate;
	}

	public List<LiteracyRateDto> getLiteracyRateDtoList() {
		return literacyRateDtoList;
	}

	public void setLiteracyRateDtoList(List<LiteracyRateDto> literacyRateDtoList) {
		this.literacyRateDtoList = literacyRateDtoList;
	}

}
