package com.example.demo.dto;

public class InspectResultsListDto {


	private String imagePath;

	private String fullName;

	private String birthday;

	private String address;

	private String grantDate;

	private String grantNumber;

	private String expirationData;

	private String licenseColor;

	private String conditions;

	private String licenseNumber;

	private String motorcycleLicense;

	private String others;

	private String commercialLicense;

	private String categorys;

	private String publicSafetyCommission;

	private String personalId;

	private String angleId;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGrantDate() {
		return grantDate;
	}

	public void setGrantDate(String grantDate) {
		this.grantDate = grantDate;
	}

	public String getGrantNumber() {
		return grantNumber;
	}

	public void setGrantNumber(String grantNumber) {
		this.grantNumber = grantNumber;
	}

	public String getExpirationData() {
		return expirationData;
	}

	public void setExpirationData(String expirationData) {
		this.expirationData = expirationData;
	}

	public String getLicenseColor() {
		return licenseColor;
	}

	public void setLicenseColor(String licenseColor) {
		this.licenseColor = licenseColor;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getMotorcycleLicense() {
		return motorcycleLicense;
	}

	public void setMotorcycleLicense(String motorcycleLicense) {
		this.motorcycleLicense = motorcycleLicense;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getCommercialLicense() {
		return commercialLicense;
	}

	public void setCommercialLicense(String commercialLicense) {
		this.commercialLicense = commercialLicense;
	}

	public String getCategorys() {
		return categorys;
	}

	public void setCategorys(String categorys) {
		this.categorys = categorys;
	}

	public String getPublicSafetyCommission() {
		return publicSafetyCommission;
	}

	public void setPublicSafetyCommission(String publicSafetyCommission) {
		this.publicSafetyCommission = publicSafetyCommission;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getAngleId() {
		return angleId;
	}

	public void setAngleId(String angleId) {
		this.angleId = angleId;
	}


}
