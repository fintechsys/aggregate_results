package com.example.demo.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * 検証結果一覧のDto
 *
 * @author naomichi nakajima
 *
 */
public class InspectionResultsJoinCorrectDataJoinAngleDto {

	private String imagePath;

	private String inspectionFullName;

	private String inspectionBirthday;

	private String inspectionAddress;

	private String inspectionGrantDate;

	private String inspectionGrantNumber;

	private String inspectionExpirationDate;

	private String inspectionLicenseColor;

	private String inspectionConditions;

	private String inspectionLicenseNumber;

	private String inspectionMotorcycleLicense;

	private String inspectionOthers;

	private String inspectionCommercialLicense;

	private String inspectionCategorys;

	private String inspectionPublicSafetyCommission;

	private String angleId;

	private String angle;

	private String personalId;

	private String correctFullName;

	private String correctBirthday;

	private String correctAddress;

	private String correctGrantDate;

	private String correctGrantNumber;

	private String correctExpirationDate;

	private String correctLicenseColor;

	private String correctConditions;

	private String correctLicenseNumber;

	private String correctMotorcycleLicense;

	private String correctOthers;

	private String correctCommercialLicense;

	private String correctCategorys;

	private String correctPublicSafetyCommission;

	private BigDecimal ratioMatch;

	private List<LiteracyRateDto> literacyRateDtoList;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getInspectionFullName() {
		return inspectionFullName;
	}

	public void setInspectionFullName(String inspectionFullName) {
		this.inspectionFullName = inspectionFullName;
	}

	public String getInspectionBirthday() {
		return inspectionBirthday;
	}

	public void setInspectionBirthday(String inspectionBirthday) {
		this.inspectionBirthday = inspectionBirthday;
	}

	public String getInspectionAddress() {
		return inspectionAddress;
	}

	public void setInspectionAddress(String inspectionAddress) {
		this.inspectionAddress = inspectionAddress;
	}

	public String getInspectionGrantDate() {
		return inspectionGrantDate;
	}

	public void setInspectionGrantDate(String inspectionGrantDate) {
		this.inspectionGrantDate = inspectionGrantDate;
	}

	public String getInspectionGrantNumber() {
		return inspectionGrantNumber;
	}

	public void setInspectionGrantNumber(String inspectionGrantNumber) {
		this.inspectionGrantNumber = inspectionGrantNumber;
	}

	public String getInspectionExpirationDate() {
		return inspectionExpirationDate;
	}

	public void setInspectionExpirationDate(String inspectionExpirationDate) {
		this.inspectionExpirationDate = inspectionExpirationDate;
	}

	public String getInspectionLicenseColor() {
		return inspectionLicenseColor;
	}

	public void setInspectionLicenseColor(String inspectionLicenseColor) {
		this.inspectionLicenseColor = inspectionLicenseColor;
	}

	public String getInspectionConditions() {
		return inspectionConditions;
	}

	public void setInspectionConditions(String inspectionConditions) {
		this.inspectionConditions = inspectionConditions;
	}

	public String getInspectionLicenseNumber() {
		return inspectionLicenseNumber;
	}

	public void setInspectionLicenseNumber(String inspectionLicenseNumber) {
		this.inspectionLicenseNumber = inspectionLicenseNumber;
	}

	public String getInspectionMotorcycleLicense() {
		return inspectionMotorcycleLicense;
	}

	public void setInspectionMotorcycleLicense(String inspectionMotorcycleLicense) {
		this.inspectionMotorcycleLicense = inspectionMotorcycleLicense;
	}

	public String getInspectionOthers() {
		return inspectionOthers;
	}

	public void setInspectionOthers(String inspectionOthers) {
		this.inspectionOthers = inspectionOthers;
	}

	public String getInspectionCommercialLicense() {
		return inspectionCommercialLicense;
	}

	public void setInspectionCommercialLicense(String inspectionCommercialLicense) {
		this.inspectionCommercialLicense = inspectionCommercialLicense;
	}

	public String getInspectionCategorys() {
		return inspectionCategorys;
	}

	public void setInspectionCategorys(String inspectionCategorys) {
		this.inspectionCategorys = inspectionCategorys;
	}

	public String getInspectionPublicSafetyCommission() {
		return inspectionPublicSafetyCommission;
	}

	public void setInspectionPublicSafetyCommission(String inspectionPublicSafetyCommission) {
		this.inspectionPublicSafetyCommission = inspectionPublicSafetyCommission;
	}

	public String getAngleId() {
		return angleId;
	}

	public void setAngleId(String angleId) {
		this.angleId = angleId;
	}

	public String getAngle() {
		return angle;
	}

	public void setAngle(String angle) {
		this.angle = angle;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getCorrectFullName() {
		return correctFullName;
	}

	public void setCorrectFullName(String correctFullName) {
		this.correctFullName = correctFullName;
	}

	public String getCorrectBirthday() {
		return correctBirthday;
	}

	public void setCorrectBirthday(String correctBirthday) {
		this.correctBirthday = correctBirthday;
	}

	public String getCorrectAddress() {
		return correctAddress;
	}

	public void setCorrectAddress(String correctAddress) {
		this.correctAddress = correctAddress;
	}

	public String getCorrectGrantDate() {
		return correctGrantDate;
	}

	public void setCorrectGrantDate(String correctGrantDate) {
		this.correctGrantDate = correctGrantDate;
	}

	public String getCorrectGrantNumber() {
		return correctGrantNumber;
	}

	public void setCorrectGrantNumber(String correctGrantNumber) {
		this.correctGrantNumber = correctGrantNumber;
	}

	public String getCorrectExpirationDate() {
		return correctExpirationDate;
	}

	public void setCorrectExpirationDate(String correctExpirationDate) {
		this.correctExpirationDate = correctExpirationDate;
	}

	public String getCorrectLicenseColor() {
		return correctLicenseColor;
	}

	public void setCorrectLicenseColor(String correctLicenseColor) {
		this.correctLicenseColor = correctLicenseColor;
	}

	public String getCorrectConditions() {
		return correctConditions;
	}

	public void setCorrectConditions(String correctConditions) {
		this.correctConditions = correctConditions;
	}

	public String getCorrectLicenseNumber() {
		return correctLicenseNumber;
	}

	public void setCorrectLicenseNumber(String correctLicenseNumber) {
		this.correctLicenseNumber = correctLicenseNumber;
	}

	public String getCorrectMotorcycleLicense() {
		return correctMotorcycleLicense;
	}

	public void setCorrectMotorcycleLicense(String correctMotorcycleLicense) {
		this.correctMotorcycleLicense = correctMotorcycleLicense;
	}

	public String getCorrectOthers() {
		return correctOthers;
	}

	public void setCorrectOthers(String correctOthers) {
		this.correctOthers = correctOthers;
	}

	public String getCorrectCommercialLicense() {
		return correctCommercialLicense;
	}

	public void setCorrectCommercialLicense(String correctCommercialLicense) {
		this.correctCommercialLicense = correctCommercialLicense;
	}

	public String getCorrectCategorys() {
		return correctCategorys;
	}

	public void setCorrectCategorys(String correctCategorys) {
		this.correctCategorys = correctCategorys;
	}

	public String getCorrectPublicSafetyCommission() {
		return correctPublicSafetyCommission;
	}

	public void setCorrectPublicSafetyCommission(String correctPublicSafetyCommission) {
		this.correctPublicSafetyCommission = correctPublicSafetyCommission;
	}

	public BigDecimal getRatioMatch() {
		return ratioMatch;
	}

	public void setRatioMatch(BigDecimal ratioMatch) {
		this.ratioMatch = ratioMatch;
	}

	public List<LiteracyRateDto> getLiteracyRateDtoList() {
		return literacyRateDtoList;
	}

	public void setLiteracyRateDtoList(List<LiteracyRateDto> literacyRateDtoList) {
		this.literacyRateDtoList = literacyRateDtoList;
	}



}
