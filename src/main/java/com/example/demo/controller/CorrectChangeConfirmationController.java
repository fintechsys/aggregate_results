package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.form.CorrectChangeConfirmationForm;
import com.example.demo.service.CorrectChangeConfirmationService;

/**
 * 正解データ変更確認のコントローラー
 * @author tujii
 *
 */
@Controller
public class CorrectChangeConfirmationController {

	@Autowired
	CorrectChangeConfirmationService correctChangeConfirmationService;


	/**
	 * 正解データ変更確認後、データをサービスに送るメソッド
	 * @param mv
	 * @param confirmationForm
	 * @return TOP画面にリダイレクト
	 */
	@RequestMapping(value = "/CorrectChangeConfirmation", method = RequestMethod.POST)
	public ModelAndView changeCorrectData(ModelAndView mv,CorrectChangeConfirmationForm confirmationForm) {

		mv.setViewName("html/CorrectChangeConfirmation");

		correctChangeConfirmationService.changeCorrectData(confirmationForm);

		return new ModelAndView("redirect:/Top");

	}

}
