package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.form.CorrectRegisterConfirmationForm;
import com.example.demo.form.CorrectRegisterForm;
import com.example.demo.service.CorrectRegisterService;
import com.example.demo.validationEdu.All;

/**
 * 正解データ登録コントローラー
 * @author tujii
 *
 */
@Controller
public class CorrectRegisterController {

	@Autowired
	CorrectRegisterService correctRegisterService;

	/**
	 * 初期画面
	 * @param mv
	 * @param form
	 * @return mv
	 */
	@RequestMapping(value = "/CorrectRegister")
	public ModelAndView CorrectRegisterDisplay(ModelAndView mv, CorrectRegisterForm form) {

		mv.setViewName("/html/CorrectRegister");

		return mv;

	}

	/**
	 * 正解データを確認画面へ送るメソッド
	 * @param mv
	 * @param form
	 * @return 正解データ登録確認画面にリダイレクト
	 */
	@RequestMapping(value = "/CorrectRegister", method = RequestMethod.POST)

	public ModelAndView setCorrectData(@Validated(All.class) CorrectRegisterForm form,
			BindingResult result, ModelAndView mv) {

		mv.setViewName("/html/CorrectRegister");

		if (result.hasErrors()) {
			return mv;
		}
		//		入力した値をFormに格納し、それをセッションにセットする

		//			session.setAttribute("correctRegister", form);
		mv.setViewName("/html/CorrectRegisterConfirmation");
		CorrectRegisterConfirmationForm confirmationForm = new CorrectRegisterConfirmationForm();
		confirmationForm.setFullName(form.getFullName());
		confirmationForm.setBirthday(form.getBirthday());
		confirmationForm.setAddress(form.getAddress());
		confirmationForm.setGrantDate(form.getGrantDate());
		confirmationForm.setGrantNumber(form.getGrantNumber());
		confirmationForm.setExpirationDate(form.getExpirationDate());
		confirmationForm.setLicenseColor(form.getLicenseColor());
		confirmationForm.setConditions(form.getConditions());
		confirmationForm.setLicenseNumber(form.getLicenseNumber());
		confirmationForm.setMotorcycleLicense(form.getMotorcycleLicense());
		confirmationForm.setOthers(form.getOthers());
		confirmationForm.setCommercialLicense(form.getCommercialLicense());
		confirmationForm.setCategorys(form.getCategorys());
		confirmationForm.setPublicSafetyCommission(form.getPublicSafetyCommission());
		mv.addObject("correctRegisterConfirmationForm", confirmationForm);

		return mv;
	}

}
