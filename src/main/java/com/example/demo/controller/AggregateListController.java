package com.example.demo.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dao.AggregateListDao;
import com.example.demo.dto.InspectionResultsJoinCorrectDataJoinAngleDto;
import com.example.demo.dto.SearchCorrectDataForAggregateListDto;
import com.example.demo.entity.AngleEntity;
import com.example.demo.form.AggregateListForm;
import com.example.demo.model.AggregateListModel;
import com.example.demo.service.AggregateListService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * 集計一覧コントローラ
 * @author naomichi nakajima
 *
 */
@Controller
public class AggregateListController {

	@Autowired
	AggregateListService aggregateListService;

	@Autowired
	ResourceLoader resource;

	/**
	 * 正解データの情報をセレクトボックスへ表示するメソッド
	 * @return correctDataDtoList
	 */
	@ModelAttribute("correctDataDtoList")
	public List<SearchCorrectDataForAggregateListDto> findByAlCorrectDto() {
		return aggregateListService.findByCorrectData();
	}

	/**
	 * 集計一覧をviewに表示させるメソッド
	 * @param form
	 * @return AggregateList
	 */
	@ModelAttribute("AggregateList")
	public List<InspectionResultsJoinCorrectDataJoinAngleDto> findByAggregateList(
			AggregateListForm form) {
		List<InspectionResultsJoinCorrectDataJoinAngleDto> DtoList = new ArrayList<>();
		if ((form.getPersonalId() == null && form.getAngleId() == null)
				|| (form.getPersonalId().isEmpty() && form.getAngleId().isEmpty())) {
			DtoList = aggregateListService.findByAggregateList();
		} else {
			DtoList = aggregateListService.searchByAggregateList(form);
		}

		return DtoList;
	}

	/**
	 * 角度データの情報をセレクトボックスへ表示するメソッド
	 * @return angleEntityList
	 */
	@ModelAttribute("angleEntityList")
	public List<AngleEntity> findByAllAngle() {
		return aggregateListService.findByAllAngle();
	}

	/**
	 * 集計一覧の初期表示
	 * @param mv
	 * @return mv
	 */
	@RequestMapping(value = "/AggregateList")
	public ModelAndView aggregateListDisplay(ModelAndView mv) {
		mv.setViewName("html/AggregateList");

		return mv;
	}

	/**
	 * 角度の情報を修正するメソッド
	 * @param mv
	 * @param form
	 * @return 集計一覧画面へリダイレクト
	 */
	@RequestMapping(value = "/AggregateList", method = RequestMethod.POST, params = "upDate")
	public ModelAndView updateAngleId(ModelAndView mv, @ModelAttribute AggregateListForm form) {
		mv.setViewName("html/AggregateList");
		aggregateListService.updateAngleId(form);

		return new ModelAndView("redirect:/AggregateList");
	}

	/**
	 * 集計一覧を角度IDと個人IDで検索するメソッド
	 * @param mv
	 * @param form
	 * @return mv
	 */
	@RequestMapping(value = "/AggregateList", method = RequestMethod.POST, params = "search")
	public ModelAndView searchByAggregateList(ModelAndView mv, AggregateListForm form) {
		mv.setViewName("html/AggregateList");

		return mv;
	}

	/**
	 * PDF出力をするメソッド
	 * @param response
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/AggregateList", method = RequestMethod.POST, params = "pdf")
	public String sample(HttpServletResponse response, AggregateListForm form) {

		List<InspectionResultsJoinCorrectDataJoinAngleDto> dtoList = new ArrayList<>();
		if ((form.getPersonalId() == null && form.getAngleId() == null)
				|| (form.getPersonalId().isEmpty() && form.getAngleId().isEmpty())) {
			dtoList = aggregateListService.findByAggregateList();
		} else {
			dtoList = aggregateListService.searchByAggregateList(form);
		}
		Date date = new Date();

		DateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 (E)");

		String Date_today = dateFormat.format(date);

		//ヘッダーデータ作成
		HashMap<String, Object> params = new HashMap<String, Object>();

		params.put("Date_today", Date_today);

		//フィールドデータ作成
		AggregateListDao dao = new AggregateListDao();
		List<AggregateListModel> fields = dao.findByAll(form, dtoList);

		//データを検索し帳票を出力
		byte[] output = OrderReporting(params, fields);

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + "AggregateList.pdf");
		response.setContentLength(output.length);

		OutputStream os = null;
		try {
			os = response.getOutputStream();
			os.write(output);
			os.flush();

			os.close();
		} catch (IOException e) {
			e.getStackTrace();
		}

		return null;
	}

	/**
	 * ジャスパーレポートコンパイル。バイナリファイルを返却する。
	 * @param data
	 * @param response
	 * @return
	 */
	private byte[] OrderReporting(HashMap<String, Object> param, List<AggregateListModel> data) {
		InputStream input;
		try {
			//帳票ファイルを取得
			input = new FileInputStream(resource.getResource("classpath:report/AggregateList.jrxml").getFile());
			//リストをフィールドのデータソースに
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(data);
			//帳票をコンパイル
			JasperReport jasperReport = JasperCompileManager.compileReport(input);

			JasperPrint jasperPrint;
			//パラメーターとフィールドデータを注入
			jasperPrint = JasperFillManager.fillReport(jasperReport, param, dataSource);
			//帳票をByte形式で出力
			return JasperExportManager.exportReportToPdf(jasperPrint);

		} catch (FileNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (JRException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

}
