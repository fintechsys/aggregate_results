package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.form.CorrectDeleteConfirmationForm;
import com.example.demo.service.CorrectDeleteConfirmationService;

/**
 * 正解データを削除確認コントローラ
 *
 * @author tsuji
 *
 */
@Controller
public class CorrectDeleteConfirmationController {

	@Autowired
	CorrectDeleteConfirmationService correctDeleteConfirmationService;

	/**
	 * 正解データを論理削除
	 * @param mv
	 * @param confirmationForm
	 * @return TOP画面へリダイレクト
	 */
	@RequestMapping(value = "/CorrectDeleteConfirmation", method = RequestMethod.POST)
	public ModelAndView deleteCorrectData(ModelAndView mv, CorrectDeleteConfirmationForm confirmationForm) {

		correctDeleteConfirmationService.deleteCorrectData(confirmationForm);

		return new ModelAndView("redirect:/Top");

	}

}
