package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.InspectionResultsJoinCorrectDataJoinAngleDto;
import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.form.InspectionResultsListForm;
import com.example.demo.service.InspectionResultsListService;

/**
 * 検証結果一覧のコントローラ
 *
 * @author naomichi nakajima
 *
 */
@Controller
public class InspectionResultsListController {

	@Autowired
	InspectionResultsListService inspectionResultsListService;

	/**
	 * 検証結果一覧を全権表示するメソッド
	 * @param form
	 * @return 検証結果DtoList
	 */
	@ModelAttribute("inspectionResultsListDtoList")
	public List<InspectionResultsJoinCorrectDataJoinAngleDto> InspectionResultsListDto() {
		return inspectionResultsListService.findByAllInspectionResults();

	}

	/**
	 *正解データの個人IDを持ってくるメソッド
	 * @return 検証結果DtoList
	 */
	@ModelAttribute("correctDataEntityList")
	public List<CorrectDataEntity> findBypersonalId() {
		return inspectionResultsListService.findByPersonalIdAndFullName();
	}

	/**
	 * 検証結果一覧の初期表示
	 * @param form
	 * @param mv
	 * @return mv
	 */
	@RequestMapping(value = "/InspectionResultsList")
	public ModelAndView InspectionResultsListDisplay(@ModelAttribute InspectionResultsListForm form,
			ModelAndView mv) {
		mv.setViewName("html/InspectionResultsList");

		return mv;
	}

	/**
	 * 検証結果データの個人IDが未入力のものを検索するメソッド
	 * @param mv
	 * @param form
	 * @return mv
	 */
	@RequestMapping(value = "/searchByEmptyPersonalId", method = RequestMethod.POST)
	public ModelAndView searchByEmptyPersonalId(ModelAndView mv, InspectionResultsListForm form) {
		mv.setViewName("html/InspectionResultsList");

		if (form.getSearchByEmpty().equals("未入力")) {
			mv.addObject("inspectionResultsListDtoList", inspectionResultsListService.findByEmptyPrsonalId());
		} else if (form.getSearchByEmpty().equals("全件")) {
			mv.addObject("inspectionResultsListDtoList", inspectionResultsListService.findByAllInspectionResults());
		}
		return mv;

	}

	/**
	 * 検証結果の個人IDを修正するメソッド
	 * @param form
	 * @param mv
	 * @return 検証結果一覧にリダイレクト
	 */
	@RequestMapping(value = "/upDatePersonalId", method = RequestMethod.POST)
	public ModelAndView updatePersonalId(@ModelAttribute InspectionResultsListForm form, ModelAndView mv) {
		mv.setViewName("html/InspectionResultsList");
		inspectionResultsListService.updatePersonalId(form);

		return new ModelAndView("redirect:/InspectionResultsList");

	}

}
