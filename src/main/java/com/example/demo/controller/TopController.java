package com.example.demo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.form.FileUploadForm;
import com.example.demo.service.TopService;

/**
 * Topコントローラ
 *
 * @author naomichi nakajima
 *
 */
@Controller
public class TopController {

	@Autowired
	TopService topservice;

	/**
	 * Top画面の初期画面
	 * @param mv
	 * @return mv
	 */
	@RequestMapping(value = "/Top")
	public ModelAndView TopDisplay(ModelAndView mv, FileUploadForm form) {
		mv.setViewName("/html/TOP");

		return mv;
	}

	/**
	 * ファイルをアップロードするメソッド
	 * @param mv
	 * @return Top画面の初期画面へリダイレクト
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ModelAndView fileUpload(ModelAndView mv, @Validated FileUploadForm form, BindingResult result) {
		mv.setViewName("/html/TOP");

		Logger log = LogManager.getLogger();
		if (result.hasErrors()) {
			log.info("エラー：ファイルが選択されていません");
			return mv;
		}
		try {
			topservice.fileUpload(form);

		} catch (Exception e) {
			log.warn("エラー：" + e);
		}

		return new ModelAndView("redirect:/Top");
	}
}
