package com.example.demo.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dao.AuthenticationNGDao;
import com.example.demo.dao.GrandTotalDao;
import com.example.demo.dto.GrandTotalDto;
import com.example.demo.dto.RecognitionResultDto;
import com.example.demo.entity.AngleEntity;
import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.form.GrandTotalForm;
import com.example.demo.model.AuthenticationNGModel;
import com.example.demo.model.GrandTotalModel;
import com.example.demo.service.GrandTotalService;
import com.example.demo.service.RecognitionResultService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * 総計コントローラ
 * @author naomichi nakajima
 *
 */
@Controller
public class GrandTotalController {

	@Autowired
	GrandTotalService grandTotalService;

	@Autowired
	RecognitionResultService recognitionResultService;


	@Autowired
	ResourceLoader resource;

	/**
	 * 正解データの情報をセレクトボックスへ表示するメソッド
	 * @return correctDataDtoList
	 */
	@ModelAttribute("correctDataEntityList")
	public List<CorrectDataEntity> findByAlCorrectDto() {
		return grandTotalService.findByCorrectData();
	}

	/**
	 * 角度データの情報をセレクトボックスへ表示するメソッド
	 * @return angleEntityList
	 */
	@ModelAttribute("angleEntityList")
	public List<AngleEntity> findByAllAngle() {
		return grandTotalService.findByAllAngle();
	}

	/**
	 * 総計結果をviewに表示させるメソッド
	 * @param form
	 * @return GrandTotal
	 */
	@ModelAttribute("grandTotalDto")
	public GrandTotalDto findByGrandTotal(
			GrandTotalForm form) {
		if (grandTotalService.findByAggregateList().isEmpty()) {
			return null;
		}
		GrandTotalDto grandTotalDto = new GrandTotalDto();
		if ((form.getPersonalId() == null && form.getAngleId() == null)
				|| (form.getPersonalId().isEmpty() && form.getAngleId().isEmpty())) {
			grandTotalDto = grandTotalService.findByAllGrandTotal();
		} else {
			grandTotalDto = grandTotalService.searchByGrandTotal(form);
		}

		return grandTotalDto;
	}

	/**
	 * 角度の名前を表示させるメソッド
	 * @param form
	 * @return categoryName
	 */
	@ModelAttribute("categoryName")
	public String categoryName(GrandTotalForm form) {
		if (form.getAngleId() == null) {
			return "全体";
		}
		return getCategoryName(form);
	}

	/**
	 * 認証結果DtoをVIEWに表示
	 * @return recognitionResultDto
	 */
	@ModelAttribute("recognitionResultDto")
	public RecognitionResultDto recognitionResultDisplay(GrandTotalForm form) {
		if (recognitionResultService.findByAllStatusData().isEmpty()) {
			return null;
		}
		return recognitionResultService.findByRecognitionResult();
	}

	/**
	 * 総計画面の初期表示
	 * @param mv
	 * @return mv
	 */
	@RequestMapping(value = "GrandTotal")
	public ModelAndView grandTotalDisplay(ModelAndView mv) {
		mv.setViewName("/html/GrandTotal");

		return mv;
	}

	/**
	 * 個人IDと角度IDをもとに検索するメソッド
	 * @param mv
	 * @return mv
	 */
	@RequestMapping(value = "/GrandTotal", method = RequestMethod.POST,params="search")
	public ModelAndView searchGrandTotal(ModelAndView mv, GrandTotalForm form) {
		mv.setViewName("/html/GrandTotal");

		return mv;
	}

	/**
	 * ファイルアップロードのメソッド
	 * @param mv
	 * @return 初期画面へリダイレクト
	 */
	@RequestMapping(value = "/GrandTotal", method = RequestMethod.POST, params = "fileUpload")
	public ModelAndView fileUpload(ModelAndView mv,@Validated  GrandTotalForm form,BindingResult result) {
		mv.setViewName("/html/GrandTotal");
	if (result.hasErrors()) {
		return mv;

	}
			recognitionResultService.fileUpload(form);

		return new ModelAndView("redirect:/GrandTotal");
	}

	/**
	 * PDF出力をするメソッド
	 * @param response
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/GrandTotal", method = RequestMethod.POST,params="AuthenticationNGpdf")
	public String AuthenticationNGDao(HttpServletResponse response, GrandTotalForm form) {

		RecognitionResultDto recognitionResultDto =recognitionResultService.findByRecognitionResult();
		Date date=new Date();

		DateFormat dateFormat=new SimpleDateFormat("yyyy年MM月dd日 (E)");

		String Date_today=dateFormat.format(date);

		//ヘッダーデータ作成
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("Date_today", Date_today);

		//フィールドデータ作成
		AuthenticationNGDao dao = new AuthenticationNGDao();
		List<AuthenticationNGModel> fields = dao.findByAll(recognitionResultDto);

		//データを検索し帳票を出力
		byte[] output = AuthenticationNGOrderReporting(params, fields);

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + "AuthenticationNG.pdf");
		response.setContentLength(output.length);

		OutputStream os = null;
		try {
			os = response.getOutputStream();
			os.write(output);
			os.flush();

			os.close();
		} catch (IOException e) {
			e.getStackTrace();
		}

		return null;
	}

	/**
	 * PDF出力をするメソッド
	 * @param response
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/GrandTotal", method = RequestMethod.POST,params="GrandTotalpdf")
	public String GrandTotalPdf(HttpServletResponse response, GrandTotalForm form) {

		GrandTotalDto grandTotalDto = new GrandTotalDto();
		if ((form.getPersonalId() == null && form.getAngleId() == null)
				|| (form.getPersonalId().isEmpty() && form.getAngleId().isEmpty())) {
			grandTotalDto = grandTotalService.findByAllGrandTotal();
		} else {
			grandTotalDto = grandTotalService.searchByGrandTotal(form);
		}
		RecognitionResultDto recognitionResultDto =recognitionResultService.findByRecognitionResult();
		Date date=new Date();

		DateFormat dateFormat=new SimpleDateFormat("yyyy年MM月dd日 (E)");

		String Date_today=dateFormat.format(date);

		//ヘッダーデータ作成
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("Date_today", Date_today);
		params.put("Category_name", getCategoryName(form));
		params.put("Number_data", grandTotalDto.getLiteracyRateDtoList().size());
		params.put("grandTotalMacthRatio", grandTotalDto.getGrandTotalMacthRatio());
		params.put("grandTotalLiteracyRate", grandTotalDto.getGrandTotalLiteracyRate());
		params.put("totalMacthRatioCount100", grandTotalDto.getTotalMacthRatioCount100());
		params.put("totalMacthRatioCount90", grandTotalDto.getTotalMacthRatioCount90());
		params.put("totalMacthRatioCount80", grandTotalDto.getTotalMacthRatioCount80());
		params.put("totalMacthRatioCountLess", grandTotalDto.getTotalMacthRatioCountLess());
		params.put("totalLiteracyRatCount100", grandTotalDto.getTotalLiteracyRatCount100());
		params.put("totalLiteracyRateCount90", grandTotalDto.getTotalLiteracyRateCount90());
		params.put("totalLiteracyRateCount80", grandTotalDto.getTotalLiteracyRateCount80());
		params.put("totalLiteracyRateCountLess", grandTotalDto.getTotalLiteracyRateCountLess());
		params.put("all", recognitionResultDto.getStatusDataDtoList().size());
		params.put("recognition", recognitionResultDto.getRecognitionResultCount());
		params.put("Percentage", recognitionResultDto.getRecognitionResultRatio());

		//フィールドデータ作成
		GrandTotalDao dao = new GrandTotalDao();
		List<GrandTotalModel> fields = dao.findByAll();

		//データを検索し帳票を出力
		byte[] output = OrderReporting(params, fields);

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + "GrandTotal.pdf");
		response.setContentLength(output.length);

		OutputStream os = null;
		try {
			os = response.getOutputStream();
			os.write(output);
			os.flush();

			os.close();
		} catch (IOException e) {
			e.getStackTrace();
		}

		return null;
	}

	/**
	 * ジャスパーレポートコンパイル。バイナリファイルを返却する。
	 * @param data
	 * @param response
	 * @return
	 */
	private byte[] OrderReporting(HashMap<String, Object> param, List<GrandTotalModel> data) {
		InputStream input;
		try {
			//帳票ファイルを取得
			input = new FileInputStream(resource.getResource("classpath:report/GrandTotal.jrxml").getFile());
			//リストをフィールドのデータソースに
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(data);
			//帳票をコンパイル
			JasperReport jasperReport = JasperCompileManager.compileReport(input);

			JasperPrint jasperPrint;
			//パラメーターとフィールドデータを注入
			jasperPrint = JasperFillManager.fillReport(jasperReport, param, dataSource);
			//帳票をByte形式で出力
			return JasperExportManager.exportReportToPdf(jasperPrint);

		} catch (FileNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (JRException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}
	/**
	 * ジャスパーレポートコンパイル。バイナリファイルを返却する。
	 * @param data
	 * @param response
	 * @return
	 */
	private byte[] AuthenticationNGOrderReporting(HashMap<String, Object> param, List<AuthenticationNGModel> data) {
		InputStream input;
		try {
			//帳票ファイルを取得
			input = new FileInputStream(resource.getResource("classpath:report/AuthenticationNG.jrxml").getFile());
			//リストをフィールドのデータソースに
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(data);
			//帳票をコンパイル
			JasperReport jasperReport = JasperCompileManager.compileReport(input);

			JasperPrint jasperPrint;
			//パラメーターとフィールドデータを注入
			jasperPrint = JasperFillManager.fillReport(jasperReport, param, dataSource);
			//帳票をByte形式で出力
			return JasperExportManager.exportReportToPdf(jasperPrint);

		} catch (FileNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (JRException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	/**
	 * 角度の名前
	 * @param form
	 * @return String
	 */
	public static String getCategoryName(GrandTotalForm form) {
		switch (form.getAngleId()) {
		case "":
			return "全体";
		case "01":
			return "真正面";
		case "02":
			return "反対正面";
		case "03":
			return "縦下";
		case "04":
			return "縦上";
		case "05":
			return "斜め右下";
		case "06":
			return "斜め右上";
		case "07":
			return "斜め左下";
		case "08":
			return "斜め左上";
		case "09":
			return "正面台形";
		case "10":
			return "反対台形";
		case "11":
			return "縦台形下";
		case "12":
			return "縦台形上";
		case "01,02":
			return "正面全体";
		case "09_10_11_12":
			return "台形全体";
		case "03,04":
			return "縦全体";
		case "05_06_07_08":
			return "斜め全体";
		}
		throw new IllegalStateException();
	}
}
