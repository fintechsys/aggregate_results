package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.CorrectDeleteDto;
import com.example.demo.form.CorrectDeleteConfirmationForm;
import com.example.demo.form.CorrectDeleteForm;
import com.example.demo.service.CorrectDeleteService;

/**
 * 正解データ削除コントローラ
 *
 * @author tsuji
 *
 */
@Controller
public class CorrectDeleteController {

	@Autowired
	CorrectDeleteService correctDeleteService;

	/**
	 * 初期画面
	 * @param personalId
	 * @param mv
	 * @param form
	 * @return mv
	 */
	@RequestMapping(value = "/CorrectDelete")
	public ModelAndView CorrectDeleteDisplay(@RequestParam("personalId") String personalId,
			ModelAndView mv, CorrectDeleteForm form) {

		mv.setViewName("html/CorrectDelete");

		CorrectDeleteDto correctDelete = correctDeleteService.correctDeleteData(personalId);

		mv.addObject("correctDelete", correctDelete);

		return mv;

	}

	/**
	 * 正解データ削除確認画面へリダイレクトするメソッド
	 * @param mv
	 * @param form
	 * @return 正解データ削除確認画面へリダイレクト
	 */
	@RequestMapping(value = "/CorrectDelete", method = RequestMethod.POST)
	public ModelAndView deleteCorrectData(ModelAndView mv, CorrectDeleteForm form) {

		mv.setViewName("html/CorrectDeleteConfirmation");

		CorrectDeleteDto correctDelete = correctDeleteService
				.correctDeleteData(form.getPersonalId());

		mv.addObject("correctDelete", correctDelete);

		CorrectDeleteConfirmationForm confirmationForm = new CorrectDeleteConfirmationForm();
		confirmationForm.setPersonalId(form.getPersonalId());
		mv.addObject("correctDeleteConfirmationForm", confirmationForm);

		return mv;

	}

}
