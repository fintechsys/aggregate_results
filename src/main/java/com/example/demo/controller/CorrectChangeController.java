package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.form.CorrectChangeConfirmationForm;
import com.example.demo.form.CorrectChangeForm;
import com.example.demo.service.CorrectChangeService;
import com.example.demo.validationEdu.All;

/**
 * 正解データ変更のコントローラー
 * @author tujii
 *
 */
@Controller
public class CorrectChangeController {

	@Autowired
	CorrectChangeService correctChangeService;

	/**
	 * 初期画面
	 * @param personalId
	 * @param mv
	 * @param form
	 * @return mv
	 */
	@RequestMapping(value = "/CorrectChange")
	public ModelAndView CorrectChangeDisplay(@RequestParam("personalId") String personalId, ModelAndView mv,
			@ModelAttribute CorrectChangeForm form) {

		mv.setViewName("html/CorrectChange");

		mv.addObject("CorrectChange", correctChangeService.correctChangeData(personalId, form));

		return mv;

	}

	/**
	 * 変更データを変更確認画面に送るメソッド
	 * @param mv
	 * @param form
	 * @param result
	 * @return 変更確認画面へリダイレクト
	 */
	@RequestMapping(value = "/CorrectChange", method = RequestMethod.POST)
	public ModelAndView changeCorrectdata(@Validated(All.class) CorrectChangeForm form,
			BindingResult result, ModelAndView mv) {

		if (result.hasErrors()) {
			return mv;
		}

		//		入力した値をFormに格納し、それをセッションにセットする

		CorrectChangeConfirmationForm confirmationForm=new CorrectChangeConfirmationForm();
		confirmationForm.setFullName(form.getFullName());
		confirmationForm.setBirthday(form.getBirthday());
		confirmationForm.setAddress(form.getAddress());
		confirmationForm.setGrantDate(form.getGrantDate());
		confirmationForm.setGrantNumber(form.getGrantNumber());
		confirmationForm.setExpirationDate(form.getExpirationDate());
		confirmationForm.setLicenseColor(form.getLicenseColor());
		confirmationForm.setConditions(form.getConditions());
		confirmationForm.setLicenseNumber(form.getLicenseNumber());
		confirmationForm.setMotorcycleLicense(form.getMotorcycleLicense());
		confirmationForm.setOthers(form.getOthers());
		confirmationForm.setCommercialLicense(form.getCommercialLicense());
		confirmationForm.setCategorys(form.getCategorys());
		confirmationForm.setPublicSafetyCommission(form.getPublicSafetyCommission());
		confirmationForm.setPersonalId(form.getPersonalId());
		mv.setViewName("html/CorrectChangeConfirmation");
		mv.addObject("correctChangeConfirmationForm", confirmationForm);

		return mv;

	}

}
