package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.CorrectListDto;
import com.example.demo.form.CorrectListForm;
import com.example.demo.service.CorrectListService;

/**
 * 正解データ一覧のコントローラー
 * @author tujii
 *
 */
@Controller
public class CorrectListController {

	@Autowired
	CorrectListService correctListService;

	/**
	 * 正解データ全件をViewに送るメソッド
	 * @return 正解データ全件
	 */
	@ModelAttribute("correctDataList")
	public List<CorrectListDto> correctDataDtoList(CorrectListForm form) {
		return correctListService.findByAllCorrectDataDtoList(form);

	}

	/**
	 * 個人IDと名前をViewに送るメソッド
	 * @return 個人IDと名前全件
	 */
	@ModelAttribute("selectList")
	public List<CorrectListDto> findByPersonalId() {
		return correctListService.selectList();
	}

	/**
	 * 初期表示
	 * @param mv
	 * @return mv
	 */
	@RequestMapping(value = "/CorrectList")
	public ModelAndView correctListDisplay(ModelAndView mv) {

		mv.setViewName("/html/correctList");

		return mv;

	}

	/**
	 * セレクトボックスで検索後の画面
	 * @param mv
	 * @return mv
	 */
	@RequestMapping(value = "/CorrectList", method = RequestMethod.POST)
	public ModelAndView searchCorrectList(ModelAndView mv) {

		mv.setViewName("/html/correctList");

		return mv;

	}

}
