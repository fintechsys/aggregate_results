package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.form.CorrectRegisterConfirmationForm;
import com.example.demo.service.CorrectRegisterConfirmationService;

/**
 * 正解データ登録確認のコントローラー
 * @author tujii
 *
 */
@Controller
public class CorrectRegisterConfirmationController {

	@Autowired
	CorrectRegisterConfirmationService correctRegisterConfirmationService;

	/**
	 * 正解データ登録確認後、データをサービスに送るメソッド
	 * @param mv
	 * @param confirmationForm
	 * @return TOP画面にリダイレクト
	 */
	@RequestMapping(value = "/CorrectRegisterConfirmation", method = RequestMethod.POST)
	public ModelAndView registerCorrectData(ModelAndView mv,
			CorrectRegisterConfirmationForm confirmationForm) {

		mv.setViewName("html/CorrectRegisterConfirmation");
		try {
			correctRegisterConfirmationService.registerCorrectData(confirmationForm);
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("Error", "これ以上登録はできません");
			mv.setViewName("html/Error");
			return mv;
		}
		return new ModelAndView("redirect:/Top");

	}

}
