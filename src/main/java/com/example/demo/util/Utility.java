package com.example.demo.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.example.demo.dto.CorrectDeleteDto;
import com.example.demo.dto.CorrectListDto;
import com.example.demo.dto.GrandTotalDto;
import com.example.demo.dto.InspectionResultsJoinCorrectDataJoinAngleDto;
import com.example.demo.dto.LiteracyRateDto;
import com.example.demo.entity.CorrectDataEntity;
import com.example.demo.entity.InspectionResultsJoinCorrectDataJoinAngleEntity;
import com.example.demo.form.CorrectChangeForm;

public class Utility {

	/**
	 * 正解データエンティティListからDtoListに置き換えるメソッド
	 * @param correctDataDtoList
	 * @param correctDataEntityList
	 */
	public static void correctDataEntityListConvertToDto(List<CorrectListDto> correctDataDtoList,
			List<CorrectDataEntity> correctDataEntityList) {

		for (CorrectDataEntity correctDataEntity : correctDataEntityList) {

			CorrectListDto correctListDto = new CorrectListDto();

			correctListDto.setPersonalId(correctDataEntity.getPersonalId());
			correctListDto.setFullName(correctDataEntity.getFullName());
			correctListDto.setBirthday(correctDataEntity.getBirthday());
			correctListDto.setAddress(correctDataEntity.getAddress());
			correctListDto.setGrantDate(correctDataEntity.getGrantDate());
			correctListDto.setGrantNumber(correctDataEntity.getGrantNumber());
			correctListDto.setExpirationDate(correctDataEntity.getExpirationDate());
			correctListDto.setLicenseColor(correctDataEntity.getLicenseColor());
			correctListDto.setConditions(correctDataEntity.getConditions());
			correctListDto.setLicenseNumber(correctDataEntity.getLicenseNumber());
			correctListDto.setMotorcycleLicense(correctDataEntity.getMotorcycleLicense());
			correctListDto.setOthers(correctDataEntity.getOthers());
			correctListDto.setCommercialLicense(correctDataEntity.getCommercialLicense());
			correctListDto.setCategorys(correctDataEntity.getCategorys());
			correctListDto.setPublicSafetyCommission(correctDataEntity.getPublicSafetyCommission());

			correctDataDtoList.add(correctListDto);

		}

	}

	/**
	 * 正解データエンティティからDtoに置き換えるメソッド
	 * @param correctDataDtoList
	 * @param correctDataEntityList
	 */
	public static void correctDataEntityConvertToDto(CorrectDeleteDto correctDeleteDto,
			CorrectDataEntity correctDataEntity) {

		correctDeleteDto.setPersonalId(correctDataEntity.getPersonalId());
		correctDeleteDto.setFullName(correctDataEntity.getFullName());
		correctDeleteDto.setBirthday(correctDataEntity.getBirthday());
		correctDeleteDto.setAddress(correctDataEntity.getAddress());
		correctDeleteDto.setGrantDate(correctDataEntity.getGrantDate());
		correctDeleteDto.setGrantNumber(correctDataEntity.getGrantNumber());
		correctDeleteDto.setExpirationDate(correctDataEntity.getExpirationDate());
		correctDeleteDto.setLicenseColor(correctDataEntity.getLicenseColor());
		correctDeleteDto.setConditions(correctDataEntity.getConditions());
		correctDeleteDto.setLicenseNumber(correctDataEntity.getLicenseNumber());
		correctDeleteDto.setMotorcycleLicense(correctDataEntity.getMotorcycleLicense());
		correctDeleteDto.setOthers(correctDataEntity.getOthers());
		correctDeleteDto.setCommercialLicense(correctDataEntity.getCommercialLicense());
		correctDeleteDto.setCategorys(correctDataEntity.getCategorys());
		correctDeleteDto.setPublicSafetyCommission(correctDataEntity.getPublicSafetyCommission());

	}

	/**
	 * エンティティからフォームに置き換えるメソッド
	 * @param correctChangeForm
	 * @param correctDataEntity
	 */
	public static void CorrectDataEntityConvertToForm(CorrectChangeForm correctChangeForm,
			CorrectDataEntity correctDataEntity) {

		correctChangeForm.setPersonalId(correctDataEntity.getPersonalId());
		correctChangeForm.setFullName(correctDataEntity.getFullName());
		correctChangeForm.setBirthday(correctDataEntity.getBirthday());
		correctChangeForm.setAddress(correctDataEntity.getAddress());
		correctChangeForm.setGrantDate(correctDataEntity.getGrantDate());
		correctChangeForm.setGrantNumber(correctDataEntity.getGrantNumber());
		correctChangeForm.setExpirationDate(correctDataEntity.getExpirationDate());
		correctChangeForm.setLicenseColor(correctDataEntity.getLicenseColor());
		correctChangeForm.setConditions(correctDataEntity.getConditions());
		correctChangeForm.setLicenseNumber(correctDataEntity.getLicenseNumber());
		correctChangeForm.setMotorcycleLicense(correctDataEntity.getMotorcycleLicense());
		correctChangeForm.setOthers(correctDataEntity.getOthers());
		correctChangeForm.setCommercialLicense(correctDataEntity.getCommercialLicense());
		correctChangeForm.setCategorys(correctDataEntity.getCategorys());
		correctChangeForm.setPublicSafetyCommission(correctDataEntity.getPublicSafetyCommission());

	}

	/**
	 * 一致率と識字率の割合ごとの件数と
	 * 総合的な一致率と識字率をDtoに格納し取得するメソッド
	 * @param inspectionResultsJoinCorrectDataJoinAngleEntitiyList
	 * @return grandTotalDto
	 */
	public static GrandTotalDto totalCount(
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {

		GrandTotalDto grandTotalDto = new GrandTotalDto();

		Integer totalMacthRatioCount100 = 0;
		Integer totalMacthRatioCount90 = 0;
		Integer totalMacthRatioCount80 = 0;
		Integer totalMacthRatioCountLess = 0;
		Integer totalLiteracyRateCount100 = 0;
		Integer totalLiteracyRateCount90 = 0;
		Integer totalLiteracyRateCount80 = 0;
		Integer totalLiteracyRateCountLess = 0;

		BigDecimal grandTotalRatioMatch = new BigDecimal(0);
		BigDecimal grandTotalLiteracyRate = new BigDecimal(0);

		for (InspectionResultsJoinCorrectDataJoinAngleEntity inspectionResultsJoinCorrectDataJoinAngleEntity : inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {

			BigDecimal ratioMatch = ratioMatch(inspectionResultsJoinCorrectDataJoinAngleEntity);
			grandTotalRatioMatch = grandTotalRatioMatch.add(ratioMatch);

			if (ratioMatch.intValue() == 100) {
				totalMacthRatioCount100++;
			} else if (ratioMatch.intValue() >= 90 && ratioMatch.intValue() < 100) {
				totalMacthRatioCount90++;
			} else if (ratioMatch.intValue() >= 80 && ratioMatch.intValue() < 90) {
				totalMacthRatioCount80++;
			} else if (ratioMatch.intValue() < 80) {
				totalMacthRatioCountLess++;
			}
		}
		for (InspectionResultsJoinCorrectDataJoinAngleEntity inspectionResultsJoinCorrectDataJoinAngleEntity : inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {

			BigDecimal literacyRate = literacyRate(inspectionResultsJoinCorrectDataJoinAngleEntity);
			grandTotalLiteracyRate = grandTotalLiteracyRate.add(literacyRate);

			if (literacyRate.intValue() == 100) {
				totalLiteracyRateCount100++;
			} else if (literacyRate.intValue() >= 90 && literacyRate.intValue() < 100) {
				totalLiteracyRateCount90++;
			} else if (literacyRate.intValue() >= 80 && literacyRate.intValue() < 90) {
				totalLiteracyRateCount80++;
			} else if (literacyRate.intValue() < 80) {
				totalLiteracyRateCountLess++;
			}

		}

		//		0で除算するとエラーが起こるため値が0の場合は除算を行わずに0と置く
		if (grandTotalRatioMatch.compareTo(BigDecimal.ZERO) == 0
				|| grandTotalLiteracyRate.compareTo(BigDecimal.ZERO) == 0) {
			grandTotalRatioMatch.compareTo(BigDecimal.ZERO);
			grandTotalLiteracyRate.compareTo(BigDecimal.ZERO);
		} else {
			grandTotalRatioMatch = grandTotalRatioMatch.divide(
					BigDecimal.valueOf(inspectionResultsJoinCorrectDataJoinAngleEntitiyList.size()), 1,
					BigDecimal.ROUND_HALF_UP);
			grandTotalLiteracyRate = grandTotalLiteracyRate.divide(
					BigDecimal.valueOf(inspectionResultsJoinCorrectDataJoinAngleEntitiyList.size()), 1,
					BigDecimal.ROUND_HALF_UP);
		}

		grandTotalDto.setTotalMacthRatioCount100(totalMacthRatioCount100);
		grandTotalDto.setTotalMacthRatioCount90(totalMacthRatioCount90);
		grandTotalDto.setTotalMacthRatioCount80(totalMacthRatioCount80);
		grandTotalDto.setTotalMacthRatioCountLess(totalMacthRatioCountLess);
		grandTotalDto.setTotalLiteracyRatCount100(totalLiteracyRateCount100);
		grandTotalDto.setTotalLiteracyRateCount90(totalLiteracyRateCount90);
		grandTotalDto.setTotalLiteracyRateCount80(totalLiteracyRateCount80);
		grandTotalDto.setTotalLiteracyRateCountLess(totalLiteracyRateCountLess);
		grandTotalDto.setGrandTotalMacthRatio(grandTotalRatioMatch);
		grandTotalDto.setGrandTotalLiteracyRate(grandTotalLiteracyRate);

		grandTotalDto.setLiteracyRateDtoList(
				LiteracyRate(inspectionResultsJoinCorrectDataJoinAngleEntitiyList));

		return grandTotalDto;
	}

	/**
	 * 正解データの項目ごとの文字数と検証結果データの項目ごとの
	 * 正解データと比較し正解した文字数の
	 * 総合した識字率をだすメソッド
	 * @param inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
	 * @return totalLiteracyRate
	 */
	public static BigDecimal literacyRate(
			InspectionResultsJoinCorrectDataJoinAngleEntity inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity) {

		double correctFullNameLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectFullName().length();
		double correctBirthdayLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectBirthday().length();
		double correctAddressLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectAddress().length();
		double correctGrantDateLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectGrantDate().length();
		double correctGrantNumberLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectGrantNumber().length();
		double correctExpirationDateLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectExpirationDate()
				.length();
		double correctLicenseColorLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectLicenseColor()
				.length();
		double correctConditionsLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectConditions().length();
		double correctLicenseNumberLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectLicenseNumber()
				.length();
		double correctMotorcycleLicenseLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectMotorcycleLicense().length();
		double correctOthersLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectOthers().length();
		double correctCommercialLicenseLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectCommercialLicense().length();
		double correctCategorysLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectCategorys().length();
		double correctPublicSafetyCommissionLength = inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
				.getCorrectPublicSafetyCommission().length();

		double fullNameCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectFullName(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionFullName());
		double birthdayCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectBirthday(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionBirthday());
		double addressCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectAddress(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionAddress());
		double GrantDateCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectGrantDate(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionGrantDate());
		double GrantNumberCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectGrantNumber(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionGrantNumber());
		double ExpirationDateCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectExpirationDate(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionExpirationDate());
		double LicenseColorCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectLicenseColor(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionLicenseColor());
		double ConditionsCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectConditions(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionConditions());
		double LicenseNumberCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectLicenseNumber(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionLicenseNumber());
		double MotorcycleLicenseCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectMotorcycleLicense(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionMotorcycleLicense());
		double OthersCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectOthers(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionOthers());
		double CommercialLicenseCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectCommercialLicense(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionCommercialLicense());
		double CategorysCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectCategorys(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionCategorys());
		double PublicSafetyCommissionCount = countAnswerLength(
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getCorrectPublicSafetyCommission(),
				inspectionResuInspectionResultsJoinCorrectDataJoinAngleEntityltsJoinCorrectDataEntity
						.getInspectionPublicSafetyCommission());

		double totalCorrectLength = correctFullNameLength + correctBirthdayLength + correctAddressLength
				+ correctGrantDateLength + correctGrantNumberLength + correctExpirationDateLength
				+ correctLicenseColorLength
				+ correctConditionsLength + correctLicenseNumberLength + correctMotorcycleLicenseLength
				+ correctOthersLength + correctCommercialLicenseLength + correctCategorysLength
				+ correctPublicSafetyCommissionLength;

		double totalCorrectAnswerLength = fullNameCount + birthdayCount + addressCount
				+ GrantDateCount + GrantNumberCount + ExpirationDateCount
				+ LicenseColorCount + ConditionsCount + LicenseNumberCount
				+ MotorcycleLicenseCount + OthersCount + CommercialLicenseCount
				+ CategorysCount + PublicSafetyCommissionCount;
		BigDecimal totalLiteracyRate = roundHerfUp(totalCorrectAnswerLength, totalCorrectLength);

		return totalLiteracyRate;
	}

	/**
	 * 識字率を求めるため正解データの項目ごとの文字数と
	 * 正解データと検証結果データを比較して正解している文字数をDtoに格納
	 * また、識字率も格納するメソッド
	 * @param inspectionResultsJoinCorrectDataJoinAngleEntitiyList
	 * @return List<LiteracyRateDto>
	 */
	public static List<LiteracyRateDto> LiteracyRate(
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {
		List<LiteracyRateDto> literacyRateDtoList = new ArrayList<>();
		for (InspectionResultsJoinCorrectDataJoinAngleEntity inspectionResultsJoinCorrectDataJoinAngleEntity : inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {
			LiteracyRateDto literacyRateDto = new LiteracyRateDto();

			//			割合を出したいためdouble型で格納している
			double correctFullNameLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectFullName()
					.length();
			double correctBirthdayLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectBirthday()
					.length();
			double correctAddressLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectAddress().length();
			double correctGrantDateLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectGrantDate()
					.length();
			double correctGrantNumberLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectGrantNumber()
					.length();
			double correctExpirationDateLength = inspectionResultsJoinCorrectDataJoinAngleEntity
					.getCorrectExpirationDate()
					.length();
			double correctLicenseColorLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectLicenseColor()
					.length();
			double correctConditionsLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectConditions()
					.length();
			double correctLicenseNumberLength = inspectionResultsJoinCorrectDataJoinAngleEntity
					.getCorrectLicenseNumber()
					.length();
			double correctMotorcycleLicenseLength = inspectionResultsJoinCorrectDataJoinAngleEntity
					.getCorrectMotorcycleLicense().length();
			double correctOthersLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectOthers().length();
			double correctCommercialLicenseLength = inspectionResultsJoinCorrectDataJoinAngleEntity
					.getCorrectCommercialLicense().length();
			double correctCategorysLength = inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectCategorys()
					.length();
			double correctPublicSafetyCommissionLength = inspectionResultsJoinCorrectDataJoinAngleEntity
					.getCorrectPublicSafetyCommission().length();

			//			文字数は整数で表示したいためDtoに格納する際にint型にキャストする
			literacyRateDto.setCorrectFullNameLength((int) correctFullNameLength);
			literacyRateDto.setCorrectBirthdayLength((int) correctBirthdayLength);
			literacyRateDto.setCorrectAddressLength((int) correctAddressLength);
			literacyRateDto.setCorrectGrantDateLength((int) correctGrantDateLength);
			literacyRateDto.setCorrectGrantNumberLength((int) correctGrantNumberLength);
			literacyRateDto.setCorrectExpirationDateLength((int) correctExpirationDateLength);
			literacyRateDto.setCorrectLicenseColorLength((int) correctLicenseColorLength);
			literacyRateDto.setCorrectConditionsLength((int) correctConditionsLength);
			literacyRateDto.setCorrectLicenseNumberLength((int) correctLicenseNumberLength);
			literacyRateDto.setCorrectMotorcycleLicenseLength((int) correctMotorcycleLicenseLength);
			literacyRateDto.setCorrectOthersLength((int) correctOthersLength);
			literacyRateDto.setCorrectCommercialLicenseLength((int) correctCommercialLicenseLength);
			literacyRateDto.setCorrectCategorysLength((int) correctCategorysLength);
			literacyRateDto.setCorrectPublicSafetyCommissionLength((int) correctPublicSafetyCommissionLength);

			//			割合を出したいためdouble型で格納している
			double fullNameCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectFullName(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionFullName());
			double birthdayCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectBirthday(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionBirthday());
			double addressCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectAddress(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionAddress());
			double GrantDateCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectGrantDate(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionGrantDate());
			double GrantNumberCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectGrantNumber(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionGrantNumber());
			double ExpirationDateCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectExpirationDate(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionExpirationDate());
			double LicenseColorCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectLicenseColor(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionLicenseColor());
			double ConditionsCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectConditions(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionConditions());
			double LicenseNumberCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectLicenseNumber(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionLicenseNumber());
			double MotorcycleLicenseCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectMotorcycleLicense(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionMotorcycleLicense());
			double OthersCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectOthers(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionOthers());
			double CommercialLicenseCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectCommercialLicense(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionCommercialLicense());
			double CategorysCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectCategorys(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionCategorys());
			double PublicSafetyCommissionCount = countAnswerLength(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectPublicSafetyCommission(),
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionPublicSafetyCommission());

			//			文字数は整数で表示したいためDtoに格納する際にint型にキャストする
			literacyRateDto.setCorrectAnswerFullNameLength((int) fullNameCount);
			literacyRateDto.setCorrectAnswerBirthdayLength((int) birthdayCount);
			literacyRateDto.setCorrectAnswerAddressLength((int) addressCount);
			literacyRateDto.setCorrectAnswerGrantDateLength((int) GrantDateCount);
			literacyRateDto.setCorrectAnswerGrantNumberLength((int) GrantNumberCount);
			literacyRateDto.setCorrectAnswerExpirationDateLength((int) ExpirationDateCount);
			literacyRateDto.setCorrectAnswerLicenseColorLength((int) LicenseColorCount);
			literacyRateDto.setCorrectAnswerConditionsLength((int) ConditionsCount);
			literacyRateDto.setCorrectAnswerLicenseNumberLength((int) LicenseNumberCount);
			literacyRateDto.setCorrectAnswerMotorcycleLicenseLength((int) MotorcycleLicenseCount);
			literacyRateDto.setCorrectAnswerOthersLength((int) OthersCount);
			literacyRateDto.setCorrectAnswerCommercialLicenseLength((int) CommercialLicenseCount);
			literacyRateDto.setCorrectAnswerCategorysLength((int) CategorysCount);
			literacyRateDto.setCorrectAnswerPublicSafetyCommissionLength((int) PublicSafetyCommissionCount);

			double totalCorrectLength = correctFullNameLength + correctBirthdayLength + correctAddressLength
					+ correctGrantDateLength + correctGrantNumberLength + correctExpirationDateLength
					+ correctLicenseColorLength
					+ correctConditionsLength + correctLicenseNumberLength + correctMotorcycleLicenseLength
					+ correctOthersLength + correctCommercialLicenseLength + correctCategorysLength
					+ correctPublicSafetyCommissionLength;

			double totalCorrectAnswerLength = fullNameCount + birthdayCount + addressCount
					+ GrantDateCount + GrantNumberCount + ExpirationDateCount
					+ LicenseColorCount + ConditionsCount + LicenseNumberCount
					+ MotorcycleLicenseCount + OthersCount + CommercialLicenseCount
					+ CategorysCount + PublicSafetyCommissionCount;

			literacyRateDto.setTotalCorrectLength((int) totalCorrectLength);
			literacyRateDto.setTotalCorrectAnswerLength((int) totalCorrectAnswerLength);

			//			項目ごとの識字率をDtoに格納
			literacyRateDto.setFullnameLiteracyRate(roundHerfUp(fullNameCount, correctFullNameLength));
			literacyRateDto.setBirthdayLiteracyRate(roundHerfUp(birthdayCount, correctBirthdayLength));
			literacyRateDto.setAddressLiteracyRate(roundHerfUp(addressCount, correctAddressLength));
			literacyRateDto.setGrantDateLiteracyRate(roundHerfUp(GrantDateCount, correctGrantDateLength));
			literacyRateDto.setGrantNumberLiteracyRate(roundHerfUp(GrantNumberCount, correctGrantNumberLength));
			literacyRateDto
					.setExpirationDateLiteracyRate(roundHerfUp(ExpirationDateCount, correctExpirationDateLength));
			literacyRateDto.setLicenseColorLiteracyRate(roundHerfUp(LicenseColorCount, correctLicenseColorLength));
			literacyRateDto.setConditionsLiteracyRate(roundHerfUp(ConditionsCount, correctConditionsLength));
			literacyRateDto.setLicenseNumberLiteracyRate(roundHerfUp(LicenseNumberCount, correctLicenseNumberLength));
			literacyRateDto.setMotorcycleLicenseLiteracyRate(
					roundHerfUp(MotorcycleLicenseCount, correctMotorcycleLicenseLength));
			literacyRateDto.setOthersLiteracyRate(roundHerfUp(OthersCount, correctOthersLength));
			literacyRateDto.setCommercialLicenseLiteracyRate(
					roundHerfUp(CommercialLicenseCount, correctCommercialLicenseLength));
			literacyRateDto.setCategorysLiteracyRate(roundHerfUp(CategorysCount, correctCategorysLength));
			literacyRateDto.setPublicSafetyCommissionLiteracyRate(
					roundHerfUp(PublicSafetyCommissionCount, correctPublicSafetyCommissionLength));
			literacyRateDto.setTotalLiteracyRate(roundHerfUp(totalCorrectAnswerLength, totalCorrectLength));

			literacyRateDtoList.add(literacyRateDto);
		}
		return literacyRateDtoList;
	}

	/**
	 * 正解データと検証結果データを比較して正解している文字数を計算するメソッド
	 * @param correct
	 * @param inspection
	 * @return count
	 */
	public static double countAnswerLength(String correct, String inspection) {
		double count = 0;
		if (correct.length() <= inspection.length()) {
			for (int i = 0; i < correct.length(); i++) {
				if (correct.charAt(i) == inspection.charAt(i)) {
					count++;
				}
			}
		} else {
			for (int i = 0; i < inspection.length(); i++) {
				if (correct.charAt(i) == inspection.charAt(i)) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * 割合を表示する際四捨五入して小数第1位まで表示させるメソッド
	 * @param ratioNum
	 * @param entireNum
	 * @return ratio
	 */
	public static BigDecimal roundHerfUp(double ratioNum, double entireNum) {
		double d = ratioNum / entireNum * 100;
		BigDecimal bd = new BigDecimal(d);
		BigDecimal ratio = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		return ratio;
	}

	/**
	 * 正解データと検証結果データを比較して同値であるものをカウントするメソッド
	 * 一致率を出すための値の確保
	 * @param inspectionResultsJoinCorrectDataJoinAngleEntity
	 * @return ratioMatch
	 */
	public static BigDecimal ratioMatch(
			InspectionResultsJoinCorrectDataJoinAngleEntity inspectionResultsJoinCorrectDataJoinAngleEntity) {
		List<String> correctData = new ArrayList<>();
		List<String> inspectionResults = new ArrayList<>();
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectFullName());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectBirthday());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectAddress());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectGrantDate());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectGrantNumber());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectExpirationDate());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectLicenseColor());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectConditions());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectLicenseNumber());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectMotorcycleLicense());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectOthers());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectCommercialLicense());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectCategorys());
		correctData.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectPublicSafetyCommission());

		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionFullName());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionBirthday());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionAddress());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionGrantDate());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionGrantNumber());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionExpirationDate());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionLicenseColor());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionConditions());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionLicenseNumber());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionMotorcycleLicense());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionOthers());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionCommercialLicense());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionCategorys());
		inspectionResults.add(inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionPublicSafetyCommission());
		double matchNum = 0;

		//		正解データと検証結果データが一致すればカウントして不一致であればカウントをしない
		//		出てきたカウントを全体で割ることで一致率を出すことができる
		for (int i = 0; i < correctData.size(); i++) {
			if (correctData.get(i).equals(inspectionResults.get(i))) {
				matchNum++;
			}
		}

		BigDecimal ratioMatch = roundHerfUp(matchNum, correctData.size());

		return ratioMatch;
	}

	/**
	 *
	 * 正解データと検証結果データと角度テーブルを結合したEntityをDtoに格納トするメソッド
	 * また文字数や識字率に必要なものをEntityから処理してDtoに格納
	 * @param inspectionResultsJoinCorrectDataJoinAngleDtoList
	 * @param inspectionResultsJoinCorrectDataJoinAngleEntitiyList
	 */
	public static void inspectionResultsJoinCorrectJoinAngleEntityConvertToDto(
			List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsJoinCorrectDataJoinAngleDtoList,
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {

		for (InspectionResultsJoinCorrectDataJoinAngleEntity inspectionResultsJoinCorrectDataJoinAngleEntity : inspectionResultsJoinCorrectDataJoinAngleEntitiyList) {
			InspectionResultsJoinCorrectDataJoinAngleDto inspectionResultsJoinCorrectDataJoinAngleDto = new InspectionResultsJoinCorrectDataJoinAngleDto();

			inspectionResultsJoinCorrectDataJoinAngleDto.setImagePath(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getImagePath());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionFullName(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionFullName());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionBirthday(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionBirthday());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionAddress(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionAddress());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionGrantDate(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionGrantDate());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionGrantNumber(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionGrantNumber());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionExpirationDate(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionExpirationDate());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionLicenseColor(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionLicenseColor());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionConditions(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionConditions());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionLicenseNumber(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionLicenseNumber());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionMotorcycleLicense(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionMotorcycleLicense());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionOthers(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionOthers());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionCommercialLicense(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionCommercialLicense());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionCategorys(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionCategorys());
			inspectionResultsJoinCorrectDataJoinAngleDto.setInspectionPublicSafetyCommission(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getInspectionPublicSafetyCommission());

			inspectionResultsJoinCorrectDataJoinAngleDto.setPersonalId(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getPersonalId());
			inspectionResultsJoinCorrectDataJoinAngleDto.setAngleId(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getAngleId());
			inspectionResultsJoinCorrectDataJoinAngleDto.setAngle(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getAngle());

			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectFullName(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectFullName());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectBirthday(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectBirthday());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectAddress(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectAddress());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectGrantDate(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectGrantDate());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectGrantNumber(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectGrantNumber());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectExpirationDate(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectExpirationDate());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectLicenseColor(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectLicenseColor());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectConditions(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectConditions());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectLicenseNumber(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectLicenseNumber());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectMotorcycleLicense(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectMotorcycleLicense());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectOthers(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectOthers());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectCommercialLicense(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectCommercialLicense());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectCategorys(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectCategorys());
			inspectionResultsJoinCorrectDataJoinAngleDto.setCorrectPublicSafetyCommission(
					inspectionResultsJoinCorrectDataJoinAngleEntity.getCorrectPublicSafetyCommission());

			inspectionResultsJoinCorrectDataJoinAngleDto.setRatioMatch(
					ratioMatch(inspectionResultsJoinCorrectDataJoinAngleEntity));

			inspectionResultsJoinCorrectDataJoinAngleDto.setLiteracyRateDtoList(
					LiteracyRate(inspectionResultsJoinCorrectDataJoinAngleEntitiyList));

			inspectionResultsJoinCorrectDataJoinAngleDtoList.add(inspectionResultsJoinCorrectDataJoinAngleDto);

		}
	}

	/**
	 * 検証結果データをエンティティからDtoにセット
	 * @param inspectionResultsListDtoList
	 * @param inspectionResultEntityList
	 */
	public static void entityConvertToDto(List<InspectionResultsJoinCorrectDataJoinAngleDto> inspectionResultsListDtoList,
			List<InspectionResultsJoinCorrectDataJoinAngleEntity> inspectionResultsJoinCorrectDataEntityList) {
		for (InspectionResultsJoinCorrectDataJoinAngleEntity inspectionResultsJoinCorrectDataEntity : inspectionResultsJoinCorrectDataEntityList) {

			InspectionResultsJoinCorrectDataJoinAngleDto inspectionResultsJoinCorrectDataDto = new InspectionResultsJoinCorrectDataJoinAngleDto();
			inspectionResultsJoinCorrectDataDto.setImagePath(inspectionResultsJoinCorrectDataEntity.getImagePath());
			inspectionResultsJoinCorrectDataDto
					.setInspectionFullName(inspectionResultsJoinCorrectDataEntity.getInspectionFullName());
			inspectionResultsJoinCorrectDataDto
					.setInspectionBirthday(inspectionResultsJoinCorrectDataEntity.getInspectionBirthday());
			inspectionResultsJoinCorrectDataDto
					.setInspectionAddress(inspectionResultsJoinCorrectDataEntity.getInspectionAddress());
			inspectionResultsJoinCorrectDataDto
					.setInspectionGrantDate(inspectionResultsJoinCorrectDataEntity.getInspectionGrantDate());
			inspectionResultsJoinCorrectDataDto
					.setInspectionGrantNumber(inspectionResultsJoinCorrectDataEntity.getInspectionGrantNumber());
			inspectionResultsJoinCorrectDataDto
					.setInspectionExpirationDate(inspectionResultsJoinCorrectDataEntity.getInspectionExpirationDate());
			inspectionResultsJoinCorrectDataDto
					.setInspectionLicenseColor(inspectionResultsJoinCorrectDataEntity.getInspectionLicenseColor());
			inspectionResultsJoinCorrectDataDto
					.setInspectionConditions(inspectionResultsJoinCorrectDataEntity.getInspectionConditions());
			inspectionResultsJoinCorrectDataDto
					.setInspectionLicenseNumber(inspectionResultsJoinCorrectDataEntity.getInspectionLicenseNumber());
			inspectionResultsJoinCorrectDataDto.setInspectionMotorcycleLicense(
					inspectionResultsJoinCorrectDataEntity.getInspectionMotorcycleLicense());
			inspectionResultsJoinCorrectDataDto
					.setInspectionOthers(inspectionResultsJoinCorrectDataEntity.getInspectionOthers());
			inspectionResultsJoinCorrectDataDto.setInspectionCommercialLicense(
					inspectionResultsJoinCorrectDataEntity.getInspectionCommercialLicense());
			inspectionResultsJoinCorrectDataDto
					.setInspectionCategorys(inspectionResultsJoinCorrectDataEntity.getInspectionCategorys());
			inspectionResultsJoinCorrectDataDto.setInspectionPublicSafetyCommission(
					inspectionResultsJoinCorrectDataEntity.getInspectionPublicSafetyCommission());
			inspectionResultsJoinCorrectDataDto.setPersonalId(inspectionResultsJoinCorrectDataEntity.getPersonalId());
			inspectionResultsJoinCorrectDataDto
					.setCorrectFullName(inspectionResultsJoinCorrectDataEntity.getCorrectFullName());

			inspectionResultsListDtoList.add(inspectionResultsJoinCorrectDataDto);
		}

	}

}
